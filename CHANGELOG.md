# Change Log

All notable changes to UnderTest.FeatureLint will be documented in this file.

## Unreleased
### Added
- adds new rule `EnsureNoEmptySteps` #178
- adds new rule `NoRepeatKeywordsInStep` #171
- adds new rule `TagCanBePromotedToTheFeatureLevel` #106
### Changed
- update `FeatureNameRequiredRule` and `NoEmptyFeatureFileRule` to return line 1 #179 
- test suite now runs cross platform #176
- corrected rule for duplicate header names across scenarios fired incorrectly #182

## 0.5.1 Sep 24, 2020
### Added
### Changed
- corrected issue where some input text causes infinite loop #175

## 0.5.0 May 6, 2020
### Added
- Adds check for features with descriptions per #113
- Adds support for `@ExternalData` on `ScenarioOutlineShouldHaveAnExampleTableRule` #149
- Adds new rule `ExampleTableHeadersDoNotIncludeAngleBracketsRule` #144
- Adds support for `--warnings-are-errors` #161
- Adds additional published package for UnderTest.FeatureLint.Common #159
- Adds new rule: no duplicate headers #164
- Adds new rule: no tab characters #117
- Adds a config command #170
### Changed
- Warnings now do not return a non-zero ExitCode by default #161 
- Single quoted text is now ignored for BadWords #148
- Corrected typo in Feature must have a name error message #142

## 0.4.2 - Dec 4, 2019
---
### Changed
- corrected issue where features with `Rule` tags would for SOE #155

## 0.4.1 - Nov 4, 2019
---
### Added
- all rules now return a Rule when in JSON mode #146

## 0.4.0 - Nov 2, 2019
---
### Added
- new rule that checks if example table headers are lower kebab case
- new rule that checks if any scenarios or backgrounds are empty #107 
- new rule that checks for tags are not on example tables #118
- new rule that checks if there is more than one when clause in scenarios #112
- add configuration #31
- add enable/disable configuration for all rules #123
- add the ability to whitelist words for `NoUpperCaseWordsRule` #124
- add builds are now announced on Slack #127
### Changed
- corrected issue where BadPhrasesRule only checked lower case string values #92

## 0.3.2 - Aug 3, 2019
### Changed
- corrected issue where BadPhrasesRule were checked for inside of quoted strings #92

## 0.3.1 - Jul 29, 2019
### Changed
- corrected issue with scenario outline rule running on scenarios #90

## 0.3.0 - Jul 27, 2019
### Added
* Added new rule for NoUpperCaseWords #55
* Added rule to check that data table headers are pascal case per #65
* Added check for example headers as parameters in scenarios per #74
* `--version` now outputs a success exitcode
### Changed
* Updated the error message for the `ScenarioOutlineShouldHaveAnExampleTableRule` rule #74
* Upgraded to Gherkin 0.6.0 #80
* Update messages when bad word found #83
* Update BadWordRule to BadPhraseRule and improve related messages #84
* improves messaging for invalid gherkin #78 
* Rules now run in alpha order #89

## 0.2.0 - Apr 7, 2019
### Added
- New rule for duplicate tags #69
- Support for output JSON from CLI #63
- New bad word `Lorem ipsum` #54
- New rule `Check for duplicate step keywords` #53
### Changed
- Updated to latest Nuke.Common for build pipeline #70  
- Fix glob when passed a folder #64

## 0.1.2 - Mar 4, 2019
### Added
- New word `Can` to bad words
### Changed

## 0.1.2 - Feb 15, 2019
### Changed
- Fix NullReferenceException on EnsureFeatureNamesAreUniqueRule

## 0.1.1 - Feb 15, 2019
### Added
- No Empty Feature File Rule

### Changed
- Fix NullReferenceException on Empty Feature File

## 0.1.0 - Jan 22, 2019

- Initial release
