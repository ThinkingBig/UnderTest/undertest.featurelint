using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using GlobExpressions;
using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.Git;
using Nuke.Common.Utilities;
using Nuke.Common.Utilities.Collections;
using UnderTest.Nuke;
using static Nuke.Common.ChangeLog.ChangelogTasks;
using static Nuke.Common.IO.FileSystemTasks;
using static Nuke.Common.IO.PathConstruction;
using static Nuke.Common.Tools.Git.GitTasks;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

[ExcludeFromCodeCoverage]
public class Build : NukeBuild
{
  public static int Main() => Execute<Build>(x => x.Pack);

  [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
  readonly string Configuration = IsLocalBuild ? "Debug" : "Release";

  [Solution("src/UnderTest.FeatureLint.sln")]
  readonly Solution Solution;

  static AbsolutePath SourceDirectory => RootDirectory / "src";

  AbsolutePath AcceptanceTestProjectDirectory = SourceDirectory / "UnderTest.FeatureLint.AcceptanceTests";

  AbsolutePath CliProjectPath = SourceDirectory / "UnderTest.FeatureLint" / "UnderTest.FeatureLint.csproj";

  AbsolutePath ArtifactsDirectory => RootDirectory / "artifacts";

  [Parameter("Api Key for Nuget.org when pushing our nuget package")]
  readonly string NugetOrgApiKey;

  const string TargetNugetServer = "https://www.nuget.org/api/v2/package";

  [Parameter]
  readonly string SymbolSource = "https://nuget.smbsrc.net/";

  List<string> ProjectsToPublishToNuget;

  string ChangelogFile => RootDirectory / "CHANGELOG.md";

  IEnumerable<string> ChangelogSectionNotes => ExtractChangelogSectionNotes(ChangelogFile);

  Target Clean => _ => _
    .Executes(() =>
    {
      GlobDirectories(SourceDirectory, "**/bin", "**/obj")
        .ForEach(DeleteDirectory);
      EnsureCleanDirectory(ArtifactsDirectory);
    });

  Target Restore => _ => _
    .DependsOn(Clean)
    .Executes(() =>
    {
      DotNetRestore(s => s
        .SetProjectFile(Solution));
    });

  Target Compile => _ => _
    .DependsOn(Restore)
    .Executes(() =>
    {
      ProjectsToPublishToNuget = new List<string>
      {
        Solution.GetProject("UnderTest.FeatureLint.Common"),
        Solution.GetProject("UnderTest.FeatureLint")
      };

      DotNetBuild(s => s
        .SetProjectFile(Solution)
        .SetConfiguration(Configuration)
        .EnableNoRestore());

      DotNetPublish(s => s
        .EnableNoRestore()
        .SetConfiguration(Configuration)
        .CombineWith(
          ProjectsToPublishToNuget,
          (settings, filename) => settings.SetProject(filename)));
    });

  public Target UnitTests =>
    _ =>
      _.DependsOn(Compile)
        .Executes(() =>
        {
          Solution.GetProjects("*.Tests")
            .ForEach(x => DotNetTest(s => s
              .SetProjectFile(x)
              .SetConfiguration(Configuration)
              .EnableNoBuild()
              .SetLogger("trx")
              .SetResultsDirectory(ArtifactsDirectory)));
        });

  public Target AcceptanceTests =>
    _ =>
      _.DependsOn(UnitTests)
        .Executes(() =>
        {
          Glob
            .Files(AcceptanceTestProjectDirectory, $"bin/**/{Configuration}/**/UnderTest.FeatureLint.AcceptanceTests.dll")
            .ForEach(x =>
            {
              UnderTestTasks.UnderTest(y => y
                .SetProjectFile(Path.Combine(AcceptanceTestProjectDirectory, x)));
            });
        });

  Target Pack => _ => _
    .DependsOn(AcceptanceTests)
    .Executes(() =>
    {
      DotNetPack(s => s
        .EnableNoBuild()
        .SetConfiguration(Configuration)
        .EnableIncludeSymbols()
        .SetOutputDirectory(ArtifactsDirectory)
        .CombineWith(ProjectsToPublishToNuget,
          (settings, filename) => settings.SetProject(filename)));
    });

  Target Push =>
    _ =>
      _.DependsOn(Pack)
        .OnlyWhenDynamic(() => OnABranchWeWantToPushToNugetOrg())
        .Requires(() => NugetOrgApiKey)
        .Requires(() => GitHasCleanWorkingCopy())
        .Requires(() => IsReleaseConfiguration())
        .Executes(() =>
        {
          GlobFiles(ArtifactsDirectory, "*.nupkg")
            .ForEach(x =>
            {
              DotNetNuGetPush(s => s
                .SetTargetPath(x)
                .SetSource("https://www.nuget.org/api/v2/package")
                .SetApiKey(NugetOrgApiKey)
                .SetLogOutput(true)
                .SetSkipDuplicate(true));
            });
        });

  public bool IsReleaseConfiguration()
  {
    return Configuration.EqualsOrdinalIgnoreCase("Release");
  }

  // our branching strategy is stable for production, release* for releases and hotfix* for hotfix items.
  public bool OnABranchWeWantToPushToNugetOrg()
  {
    var branch = this.GetCurrentBranch().ToLowerInvariant();
    Logger.Log(LogLevel.Normal, $"Current branch {branch}");

    return branch == "stable"
           || branch.StartsWith("release")
           || branch.StartsWith("hotfix");
  }

  string GetCurrentBranch()
  {
    var environmentVariable = Environment.GetEnvironmentVariable("APPVEYOR_REPO_BRANCH");
    if (!string.IsNullOrEmpty(environmentVariable))
    {
      return environmentVariable;
    }

    return GitTasks.GitCurrentBranch();
  }
}
