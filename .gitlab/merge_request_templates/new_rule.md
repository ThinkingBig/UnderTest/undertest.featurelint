
* [ ] PrecedenceBasedOptionsBuilder - add hading for the config
* [ ] add rule to `src/UnderTest.FeatureLint.Common.Rules`
* [ ] mark the folder as not a `Namespace provider`
* [ ] add config class to same location
* [ ] add a property for the new rule to `RuleSettings`
* [ ] add the rule to `RuleType`
* [ ] unit test your rule
* [ ] if it is unique in approach, add an acceptance test
* [ ] add the rule to the README.md under Rules
* [ ] bump nuget version
* [ ] add to CHANGELOG
