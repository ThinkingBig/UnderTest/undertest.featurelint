**Tell us about your environment:** 

* ***FeatureLint version***  `featurelint --version`

**Please show your full configuration:**

<details>
<summary>Configuration</summary>

<!-- Paste your configuration below: -->
```bash

```

</details>

**What happened? Please include the actual source code causing the issue, as well as the command that you used to run FeatureLint.**

<!-- Paste the source code below: -->
```gherkin

```

<!-- Paste the command you used to run FeatureLint: -->
```bash

```

**What did you expect to happen?**


**What actually happened? Please include the actual, raw output from FeatureLint.**

/label ~new-rule ~triage
/assign @RonMyers

