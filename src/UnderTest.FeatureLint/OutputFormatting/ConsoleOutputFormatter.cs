using System.Drawing;
using System.Linq;
using Colorful;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Extensions;
using UnderTest.FeatureLint.Common.OutputFormatting;

namespace UnderTest.FeatureLint.OutputFormatting
{
  public class ConsoleOutputFormatter : IOutputFormatter
  {
    public string BuildErrorResult(FeatureLintResult result)
    {
      Console.WriteLine();
      Console.WriteLine();

      foreach (var message in result.InfoMessages)
      {
        Console.WriteLine(message);
      }

      foreach (var message in result.ErrorMessages)
      {
        Console.WriteLine(message);
      }

      var fileFindings = result.GroupByFilename();

      foreach (var item in fileFindings)
      {
        Console.WriteLine(item.FilePath, Color.White);
        var orderedFindings = item.Findings.OrderBy(x => x.Line);
        foreach (var finding in orderedFindings)
        {
          var outputColoring = new[]
          {
            new Formatter(finding.Line, Color.White),
            new Formatter(finding.Column, Color.White),
            new Formatter(finding.Type, Color.DarkRed),
            new Formatter(finding.Message, Color.Gray),
          };

          Console.WriteLineFormatted(" {0}:{1} {2} - {3}", Color.White, outputColoring);
        }
      }

      Console.WriteLine();
      Console.WriteLine($" X {result.Summary.TotalFindings} problems ({result.Summary.Errors} errors, {result.Summary.Warnings} warnings)", Color.DarkRed);

      return string.Empty;
    }

    public string BuildSuccessResult()
    {
      return $"All good in the hood";
    }
  }
}
