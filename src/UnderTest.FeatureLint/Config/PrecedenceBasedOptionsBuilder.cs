using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Config
{
  internal class PrecedenceBasedOptionsBuilder
  {
    /// <summary>
    /// The order of precedence for options is as follows:
    ///   1. in the $HOME/.featurelint
    ///   2. .featurelint in a parent folder from .
    ///   3. in ./.featurelint
    ///   4. on the command line (not complete)
    /// </summary>
    public PrecedenceBasedOptionsBuilder(FeatureLintResult resultP)
    {
      result = resultP;
      resultingSettings = new DefaultRuleSettingsFactory().GetInstance();

      WithHomeDirectoryConfigValues();
      WithParentDirectoryConfigValues();
      WithCurrentDirectoryConfigValues();
    }

    private const string FeatureLintFilename = ".featurelint";

    private readonly IFeatureLintConfig resultingSettings;

    private readonly FeatureLintResult result;

    public IFeatureLintConfig Build()
    {
      return resultingSettings;
    }

    public PrecedenceBasedOptionsBuilder WithCommandLine(Options commandLineOptions)
    {
      resultingSettings.FilePaths = commandLineOptions.FilePaths;
      resultingSettings.IgnoreTags = commandLineOptions.IgnoreTags;

      return this;
    }

    private void ExtendSettingsWith(JObject config)
    {
      if (config == null)
      {
        return;
      }

      resultingSettings.IgnoreTags = config.ReadValueOrDefault("ignoreTags", new List<string>());

      var ruleSettings = config["ruleSettings"];

      if (ruleSettings == null)
      {
        return;
      }

      // todo - lets refactor this to be call a method on the rule config with the goal of increasing the locality of change
      HandleBadPhrasesRuleConfig(ruleSettings)
        .HandleDataTableHeadersUsePascalCaseRuleConfig(ruleSettings)
        .HandleEnsureFeatureNamesAreUniqueRuleConfig(ruleSettings)
        .HandleEnsureScenarioNamesAreUniqueRuleConfig(ruleSettings)
        .HandleExampleTablesShouldUseLowerKebabCaseRuleConfig(ruleSettings)
        .HandleFeatureNameRequiredRuleConfig(ruleSettings)
        .HandleNoDuplicateTagsRuleConfig(ruleSettings)
        .HandleNoDuplicateWhenGivenThenRuleConfig(ruleSettings)
        .HandleNoEmptyFeatureFileRuleConfig(ruleSettings)
        .HandleNoEmptyScenarioRuleConfig(ruleSettings)
        .HandleNoPeriodsEndingLinesRuleConfig(ruleSettings)
        .HandleNoTagsOnExampleTablesRuleConfig(ruleSettings)
        .HandleNoThenInBackgroundsRuleConfig(ruleSettings)
        .HandleNoUpperCaseWordsRuleConfig(ruleSettings)
        .HandleNoEmptyStepRuleConfig(ruleSettings)
        .HandleNoRepeatKeywordsRuleConfig(ruleSettings)
        .HandleOnlyOneWhenPerScenarioRuleConfig(ruleSettings)
        .HandleExampleTableHeadersAreUniqueRuleConfig(ruleSettings)
        .HandleOrderOfGivenWhenThenRuleConfig(ruleSettings)
        .HandleScenarioNameRequiredRuleConfig(ruleSettings)
        .HandleScenarioOutlineShouldHaveAnExampleTableRuleConfig(ruleSettings)
        .HandleScenarioOutlineExamplesShouldHaveANameRuleConfig(ruleSettings)
        .HandleUnmappedExampleElementsRuleConfig(ruleSettings)
        .HandleTagCanBePromotedToTheFeatureLevelRuleConfig(ruleSettings)
        .HandleFeatureMustHaveDescriptionRuleConfig(ruleSettings);
    }

    private PrecedenceBasedOptionsBuilder HandleTagCanBePromotedToTheFeatureLevelRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.TagCanBePromotedToTheFeatureLevel, nameof(resultingSettings.RuleSettings.TagCanBePromotedToTheFeatureLevel).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoRepeatKeywordsRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoRepeatedKeywordsInStep, nameof(resultingSettings.RuleSettings.NoRepeatedKeywordsInStep).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoEmptyStepRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoEmptySteps, nameof(resultingSettings.RuleSettings.NoEmptySteps).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleFeatureMustHaveDescriptionRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.FeatureMustHaveDescription,
                                                      nameof(resultingSettings.RuleSettings.FeatureMustHaveDescription).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleUnmappedExampleElementsRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.UnmappedExampleElements,
                                                nameof(resultingSettings.RuleSettings.UnmappedExampleElements).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleScenarioOutlineExamplesShouldHaveANameRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.ScenarioOutlineExamplesShouldHaveAName,
                                          nameof(resultingSettings.RuleSettings.ScenarioOutlineExamplesShouldHaveAName).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleScenarioOutlineShouldHaveAnExampleTableRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.ScenarioOutlineShouldHaveAnExampleTable,
                                    nameof(resultingSettings.RuleSettings.ScenarioOutlineShouldHaveAnExampleTable).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleScenarioNameRequiredRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.ScenarioNameRequired,
                                    nameof(resultingSettings.RuleSettings.ScenarioNameRequired).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleOrderOfGivenWhenThenRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.OrderOfGivenWhenThen,
                                    nameof(resultingSettings.RuleSettings.OrderOfGivenWhenThen).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleOnlyOneWhenPerScenarioRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.OnlyOneWhenPerScenario,
                                    nameof(resultingSettings.RuleSettings.OnlyOneWhenPerScenario).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoUpperCaseWordsRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoUpperCaseWords,
                                    nameof(resultingSettings.RuleSettings.NoUpperCaseWords).ToCamelCase(),
                                    (config, token) =>
                                    {
                                      config.WhiteListedWords =
                                        token.ReadValueOrDefault("whitelistedWords", config.WhiteListedWords);
                                    });

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoThenInBackgroundsRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoThenInBackgrounds,
                                    nameof(resultingSettings.RuleSettings.NoThenInBackgrounds).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoTagsOnExampleTablesRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoTagsOnExampleTables,
                                    nameof(resultingSettings.RuleSettings.NoTagsOnExampleTables).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoPeriodsEndingLinesRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoPeriodsEndingLines,
                                    nameof(resultingSettings.RuleSettings.NoPeriodsEndingLines).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoEmptyFeatureFileRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoEmptyFeatureFile,
                                    nameof(resultingSettings.RuleSettings.NoEmptyFeatureFile).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoEmptyScenarioRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoEmptyScenario, nameof(resultingSettings.RuleSettings.NoEmptyScenario));

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoDuplicateWhenGivenThenRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoDuplicateWhenGivenThenRule,
                                    nameof(resultingSettings.RuleSettings.NoDuplicateWhenGivenThenRule).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleNoDuplicateTagsRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.NoDuplicateTagsRule,
                                    nameof(resultingSettings.RuleSettings.NoDuplicateTagsRule).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleFeatureNameRequiredRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.FeatureNameRequiredRule,
                                    nameof(resultingSettings.RuleSettings.FeatureNameRequiredRule).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleExampleTablesShouldUseLowerKebabCaseRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.ExampleTablesShouldUseLowerKebabCase,
                                    nameof(resultingSettings.RuleSettings.ExampleTablesShouldUseLowerKebabCase).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleExampleTableHeadersAreUniqueRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.ExampleTableHeadersAreUniqueRule,
        nameof(resultingSettings.RuleSettings.ExampleTableHeadersAreUniqueRule).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleEnsureScenarioNamesAreUniqueRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.EnsureScenarioNamesAreUnique,
                                    nameof(resultingSettings.RuleSettings.EnsureScenarioNamesAreUnique).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleEnsureFeatureNamesAreUniqueRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.EnsureFeatureNamesAreUnique,
                                    nameof(resultingSettings.RuleSettings.EnsureFeatureNamesAreUnique).ToCamelCase());

      return this;
    }

    private PrecedenceBasedOptionsBuilder HandleDataTableHeadersUsePascalCaseRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.DataTableHeadersUsePascalCase,
                                    nameof(resultingSettings.RuleSettings.DataTableHeadersUsePascalCase).ToCamelCase());

      return this;
    }

    private JObject AttemptToLoadConfigValuesFromFile(string filename)
    {
      try
      {
        var serializerSettings = new JsonSerializerSettings
        {
          ContractResolver = new CamelCasePropertyNamesContractResolver()
        };

        return JsonConvert.DeserializeObject<JObject>(File.ReadAllText(filename), serializerSettings);
      }
      catch
      {
        return null;
      }
    }

    private PrecedenceBasedOptionsBuilder HandleBadPhrasesRuleConfig(JToken ruleSettings)
    {
      ruleSettings.HandleRuleConfig(resultingSettings.RuleSettings.BadPhrases,
        nameof(resultingSettings.RuleSettings.BadPhrases).ToCamelCase(),
        (config, token) =>
      {
        config.Untestable = token.ReadValueOrDefault("untestable", config.Untestable);
        config.Untestable.AddRange(token.ReadValueOrDefault("additionalUntestable", new List<string>()));
        config.VaguePhrases = token.ReadValueOrDefault("vaguePhrases", config.VaguePhrases);
        config.VaguePhrases.AddRange(token.ReadValueOrDefault("additionalVaguePhrases", new List<string>()));
      });

      return this;
    }

    private void WithCurrentDirectoryConfigValues()
    {
      var filename = Path.Combine(Directory.GetCurrentDirectory(), FeatureLintFilename);
      var config = AttemptToLoadConfigurationFile(filename);

      ExtendSettingsWith(config);
    }

    private void WithHomeDirectoryConfigValues()
    {
      var filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile), FeatureLintFilename);
      var config = AttemptToLoadConfigurationFile(filename);

      ExtendSettingsWith(config);
    }

    private void WithParentDirectoryConfigValues()
    {
      var dir = new DirectoryInfo(Directory.GetCurrentDirectory()).Parent;
      while (dir.Name != dir.Root.Name)
      {
        var filename = Path.Combine(dir.FullName, FeatureLintFilename);
        var config = AttemptToLoadConfigurationFile(filename);

        if (config == null)
        {
          dir = dir.Parent;
          continue;
        }

        ExtendSettingsWith(config);
        break;
      }
    }

    private JObject AttemptToLoadConfigurationFile(string filename)
    {
      try
      {
        if (!File.Exists(filename))
        {
          return null;
        }

        result.InfoMessages.Add($"extending configuration with {filename}");

        return AttemptToLoadConfigValuesFromFile(filename);
      }
      catch (Exception err)
      {
        result.ErrorMessages.Add($"There was an error loading the configuration file ${filename} with the message: ${err.Message}");
        return null;
      }
    }
  }
}
