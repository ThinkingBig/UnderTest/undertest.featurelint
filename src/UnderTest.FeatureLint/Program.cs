using System.Collections.Generic;
using System.Linq;
using CommandLine;
using JetBrains.Annotations;
using Newtonsoft.Json;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.OutputFormatting;
using UnderTest.FeatureLint.Config;
using UnderTest.FeatureLint.OutputFormatting;
using Console = Colorful.Console;

namespace UnderTest.FeatureLint
{
  [PublicAPI]
  public class Program
  {
    static int Main(string[] args)
    {
      Console.OutputEncoding = System.Text.Encoding.UTF8;

      return Parser.Default.ParseArguments<Options>(args)
        .MapResult(
          options => (int)RunAndReturnExitCode(options),
          HandleNonParsed);
    }

    private static int HandleNonParsed(IEnumerable<Error> _)
    {
      if (_.Any(e => e is VersionRequestedError))
      {
        return (int) AppExitCode.Success;
      }

      return (int) AppExitCode.InvalidArguments;
    }

    private static AppExitCode RunAndReturnExitCode(Options commandLineOptions)
    {
      var result = new FeatureLintResult();
      var options = new PrecedenceBasedOptionsBuilder(result)
        .WithCommandLine(commandLineOptions)
        .Build();

      if (DisplayConfigIfRequested(commandLineOptions, options))
      {
        return AppExitCode.Success;
      }

      new FeatureLintRunner(options, result)
                      .Execute();

      var output = GetOutputFormatterFromOptions(commandLineOptions);
      if (!result.Findings.Any())
      {
        Console.WriteLine(output.BuildSuccessResult());
        return AppExitCode.Success;
      }

      Console.WriteLine(output.BuildErrorResult(result));

      return BuildExitCode(commandLineOptions, result);
    }

    private static AppExitCode BuildExitCode(Options commandLineOptions, FeatureLintResult result)
    {
      if (commandLineOptions.WarningsAreErrors)
      {
        return AppExitCode.ErrorsFound;
      }

      if (result.Findings.Any(x => x.Type != FeatureLintDataType.Warning))
      {
        return AppExitCode.ErrorsFound;
      }

      return AppExitCode.Success;
    }

    private static bool DisplayConfigIfRequested(Options commandLineOptions, IFeatureLintConfig options)
    {
      if (!commandLineOptions.OutputConfig)
      {
        return false;
      }

      var config = JsonConvert.SerializeObject(options, Formatting.Indented);
      Console.WriteLine(config);

      return true;
    }

    private static IOutputFormatter GetOutputFormatterFromOptions(Options options)
    {
      if (options.JsonOutput)
      {
        return new JsonOutputFormatter();
      }

      return new ConsoleOutputFormatter();
    }
  }
}
