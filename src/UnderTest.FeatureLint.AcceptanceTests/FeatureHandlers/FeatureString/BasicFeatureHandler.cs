using FluentAssertions;
using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.FeatureString
{
  [HandlesFeature("FeatureString/Basic.feature")]
  public class BasicFeatureHandler: FeatureHandler
  {
    private string content;
    private FeatureLintResult result;

    [Given("a <validity> gherkin string")]
    public void AValidGherkinString([ValidStepParam]bool validity)
      => content = this.LoadAssetsFeatureFile(validity ? "ValidGherkin.feature" : "InvalidGherkin.feature");

    [When("I lint the contents of the string")]
    public void ILintTheContentOfTheString()
      => RunFeatureLint();

    [Then("the result should be <expected-result>")]
    public void TheResultShouldBeSuccessful([SuccessfulStepParam]bool expected)
      => result.ValidGherkin.Should().Be(expected);

    private void RunFeatureLint()
    {
      var config = new DefaultRuleSettingsFactory().GetInstance();
      var runner = new FeatureLintStringLinter(config);
      result = runner.Execute(content);
    }
  }
}
