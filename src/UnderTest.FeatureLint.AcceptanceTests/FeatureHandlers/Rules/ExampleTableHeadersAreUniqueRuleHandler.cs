﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ExampleTableHeadersAreUniqueRule.feature")]
  public class ExampleTableHeadersAreUniqueRuleHandler: BaseRuleHandler<ExampleTableHeadersAreUniqueRule>
  {
    [Given("an Example Table with a header (with|without) duplicate headers")]
    public void AnExampleTableWithAHeaderWithDuplicateHeaders([WithOrWithoutStepParam]bool withDuplicates)
      => UpdateContentWithPotentialExampleTableHeaderDuplicates(withDuplicates);

    private void UpdateContentWithPotentialExampleTableHeaderDuplicates(bool hasDuplicate)
    {
      var content = $@"
Feature: A feature

  Scenario Outline: A scenario
  Given the a something
  When something happens
  Then these new things are true
Examples: Headers
";

      if (hasDuplicate)
      {
        content += " | header | header |";
      }
      else
      {
        content += " | header | another-header |";
      }

      // note: headers are only visible if they have rows
      content += "\n| | |";

      Content = content;
    }
  }
}
