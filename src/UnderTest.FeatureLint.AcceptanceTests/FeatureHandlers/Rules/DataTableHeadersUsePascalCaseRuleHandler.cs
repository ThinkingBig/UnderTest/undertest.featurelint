using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/DataTableHeadersUsePascalCaseRule.feature")]
  public class DataTableHeadersUsePascalCaseRuleHandler: BaseRuleHandler<DataTableHeadersUsePascalCaseRule>
  {
    [Given("a data table with a header not in pascal case: (.*)")]
    public void ADatatableWithAHeaderNotInPascalCase(string header)
      => UpdateContentWithPotentialDataTableHeader(header);

    [Given("a data table with all headers in pascal case: (.*)")]
    public void ADataTableWithAllHeadersInPascalCase(string header)
      => UpdateContentWithPotentialDataTableHeader(header);

    private void UpdateContentWithPotentialDataTableHeader(string header)
    {
      Content = $@"
Feature: A feature

  Scenario: A scenario
  Given the following information:
   | {header} |
  When something happens
  Then these new things are true";
    }
  }
}
