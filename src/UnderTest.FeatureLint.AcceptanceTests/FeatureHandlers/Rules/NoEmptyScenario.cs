using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoEmptyScenarioRule.feature")]
  public class NoEmptyScenario : BaseRuleHandler<NoEmptyScenarioRule>
  {
    [Given("a scenario with steps")]
    public void AScenarioWithSteps()
    {
      Content = @"
Feature: Placeholder Feature title
  A description

Scenario: something
  Given a step";
    }

    [Given("a feature file with a scenario with no steps")]
    public void AScenarioWithNoSteps()
    {
      Content = @"
Feature: something
  A description

Scenario: I am empty";
    }

    [Given("a feature file with a background with no steps")]
    public void ABackgroundWithNoSteps()
    {
      Content = @"
Feature: something
  A description

Background: I am empty";
    }
  }
}
