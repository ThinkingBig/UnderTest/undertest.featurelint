﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoTabCharacters.feature")]
  public class NoTabCharacters : BaseRuleHandler<NoTabCharactersRule>
  {
    [Given("a feature file with a tab character")]
    public void AFeatureFileWithATitle() =>
      Content = @"
Feature: Placeholder Feature title

  Scenario: something
    When something " + "\t" + " something";
  }
}
