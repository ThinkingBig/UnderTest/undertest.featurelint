using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/NoEmptyFeatureFile.feature")]
  public class NoEmptyFeatureFile : BaseRuleHandler<NoEmptyFeatureFileRule>
  {
    [Given("a feature file with a title")]
    public void AFeatureFileWithATitle()
      => Content = "Feature: Placeholder Feature title";

    [Given("a feature file with no content")]
    public void AFeatureFileWithNoContent()
      => Content = string.Empty;
  }
}
