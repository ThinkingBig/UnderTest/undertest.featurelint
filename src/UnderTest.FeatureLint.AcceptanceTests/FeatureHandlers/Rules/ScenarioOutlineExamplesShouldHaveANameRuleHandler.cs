﻿using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.Common;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.FeatureHandlers.Rules
{
  [HandlesFeature(@"Rules/ScenarioOutlineExamplesShouldHaveANameRule.feature")]
  public class ScenarioOutlineExamplesShouldHaveANameRuleHandler
    : BaseRuleHandler<ScenarioOutlineExamplesShouldHaveANameRule>
  {
    [Given("an example table (with|without) a name")]
    public void AnExampleTableWithAName([WithOrWithoutStepParam] bool withName)
      => UpdateContentWithExampleTableWithRows(withName);

    private void UpdateContentWithExampleTableWithRows(bool hasAStepWithAPeriod)
    {
      var name = hasAStepWithAPeriod ? "name" : string.Empty;

      Content = $@"
Feature: A feature

  Scenario Outline: A scenario
    Given the a foo>
    When something happens
    Then these new things are true

    Examples: {name}
      | header |
      |        |
";
    }
  }
}
