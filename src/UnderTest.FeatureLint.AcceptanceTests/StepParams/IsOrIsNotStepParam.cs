﻿using System;
using UnderTest.Attributes;

namespace UnderTest.FeatureLint.AcceptanceTests.StepParams
{
  public class IsOrIsNotStepParam: StepParamAttribute
  {
    public override object Transform(object input)
    {
      var text = ((string) input).ToLowerInvariant().Trim();
      switch (text)
      {
        case "is":
          return true;
        case "is not":
          return false;
        default:
          throw new NotImplementedException($"Phrase not recognized. Expected 'is' or 'is not'. Got: {text}");
      }
    }
  }
}
