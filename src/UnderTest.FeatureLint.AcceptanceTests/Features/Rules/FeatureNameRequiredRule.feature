Feature: Feature Name Required Rule

All features should have titles.

Scenario: Feature does not have a title
  Given a feature that does not have a title
  When I lint the feature file
  Then the `FeatureNameRequiredRule` error is given

Scenario: Feature does have a title
  Given a feature that has a title
  When I lint the feature file
  Then the `FeatureNameRequiredRule` error is not given
