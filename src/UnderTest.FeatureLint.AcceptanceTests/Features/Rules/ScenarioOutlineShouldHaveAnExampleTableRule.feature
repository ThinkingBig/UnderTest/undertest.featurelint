Feature: Scenario Outline Should Have An Example Table Rule

If the keyword `Scenario Outline` is used, an example table should be provided for the scenario outline.

Scenario: Scenario outline without an example table
  Given a scenario outline without an example table
  When I lint the feature file
  Then the `ScenarioOutlineShouldHaveAnExampleTableRule` error is given

Scenario: Scenario outline with an example table
  Given a scenario outline with an example table
  When I lint the feature file
  Then the `ScenarioOutlineShouldHaveAnExampleTableRule` error is not given
