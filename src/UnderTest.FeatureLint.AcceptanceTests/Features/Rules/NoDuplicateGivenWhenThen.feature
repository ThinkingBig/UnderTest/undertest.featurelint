Feature: No Duplicate Given When Then

 The keywords `Given`, `When`, `Then` should not be used twice or more in a row.
 Where duplicate steps might be used, the `And` or `But` keyword should be used instead.

  Scenario Outline: Scenario uses step keyword more than once in a row
    Given a feature file with two <step-keyword> steps in a row
    When I lint the feature file
    Then the `NoDuplicateKeyWord` error is given

    Examples: Step keywords
      | step-keyword |
      | Given        |
      | When         |
      | Then         |

  Scenario: Scenario does not have steps using a keyword twice in a row
    Given a scenario with only one instance of the keywords Given, When, Then
    When I lint the feature file
    Then the `NoDuplicateKeyWord` error is not given
