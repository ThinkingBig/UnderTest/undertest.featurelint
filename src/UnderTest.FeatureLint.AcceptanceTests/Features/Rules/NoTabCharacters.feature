﻿Feature: No Empty Feature File

Feature files should never not have content in the feature file.

  Scenario: Feature has a tab character
    Given a feature file with a tab character
    When I lint the feature file
    Then the `NoTabCharacter` error is given
