﻿Feature: Example Tables Should Use Lower Kebab Case

  To ease reading, tables should be have a consistent naming style.

  Scenario: Example Table has duplicate headers
    Given an Example Table with a header with duplicate headers
    When I lint the feature file
    Then the `ExampleTableHeadersAreUniqueRule` error is given

  Scenario: Example Table without duplicate headers
    Given an Example Table with a header without duplicate headers
    When I lint the feature file
    Then the `ExampleTableHeadersAreUniqueRule` error is not given
