Feature: Scenario Outline Examples Should Have a Name Rule

Example tables should have a name to describe the type or category of examples being provided for a scenario outline.

Scenario: Without a name
  Given an example table without a name
  When I lint the feature file
  Then the `ScenarioOutlineExamplesShouldHaveANameRule` error is given

Scenario: With a name
  Given an example table with a name
  When I lint the feature file
  Then the `ScenarioOutlineExamplesShouldHaveANameRule` error is not given
