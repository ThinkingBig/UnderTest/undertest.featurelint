Feature: Basic Feature String

  As a developer
  I want to be able to lint a string
  So that I can process gherkin without saving to a file

  Scenario Outline: Pass successful string
    Given a <validity> gherkin string
    When I lint the contents of the string
    Then the result should be <expected-result>

    Examples:
    | validity | expected-result |
    | invalid  | failure         |
    | valid    | successful      |
