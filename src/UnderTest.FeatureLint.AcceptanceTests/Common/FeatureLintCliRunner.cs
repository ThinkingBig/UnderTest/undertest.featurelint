﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Serilog;
using UnderTest.FeatureLint.Common.OutputFormatting;

namespace UnderTest.FeatureLint.AcceptanceTests.Common
{
  public class FeatureLintCliRunner
  {
    public FeatureLintJsonResult RunFeatureLintCliWithContent(string content)
    {
      try
      {
        var filename = WriteContentToTempFilename(content);
        var output = RunFeatureLintCliAndCaptureOutput(filename);

        return JsonConvert.DeserializeObject<FeatureLintJsonResult>(output);
      }
      catch (Exception e)
      {
        Log.Error($"Execution of `featurelint` failed with message: {e.Message}");
        throw;
      }
    }

    private static string RunFeatureLintCliAndCaptureOutput(string filename)
    {
      var output = string.Empty;
      var featureLintArgs = $"--json {filename}";
      var debugRelease =
#if DEBUG
        "Debug";
#else
        "Release";
#endif

      var process = new Process
      {
        StartInfo =
        {
          FileName = "dotnet",
          Arguments = $"exec UnderTest.FeatureLint.dll -- {featureLintArgs}",
          UseShellExecute = false,
          RedirectStandardOutput = true,
          // this relative path is not the most elegant approach but this project is in the same repo and we want the latest version
          // create an issue/MR in the repo if you have a improved approach
          WorkingDirectory = Path.Combine(
            Path.GetDirectoryName(Assembly.GetAssembly(typeof(FeatureLintCliRunner)).Location),
            "..", "..", "..", "..", "UnderTest.FeatureLint", "bin", debugRelease, "netcoreapp2.1")
        }
      };
      process.OutputDataReceived += (sender, args) => output += args.Data;
      process.Start();
      process.BeginOutputReadLine();
      process.WaitForExit();
      return output;
    }

    private string WriteContentToTempFilename(string content)
    {
      var tempDirectoryToTestIn = Path.Combine(Path.GetTempPath(), "FeatureLint.AcceptanceTests", Path.GetRandomFileName());
      if (!Directory.Exists(tempDirectoryToTestIn))
      {
        Directory.CreateDirectory(tempDirectoryToTestIn);
      }

      var filename = Path.Combine(tempDirectoryToTestIn, Path.GetRandomFileName());
      File.WriteAllText(filename, content);

      return filename;
    }
  }
}
