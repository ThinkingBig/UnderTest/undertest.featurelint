using JetBrains.Annotations;
using UnderTest.Attributes;
using UnderTest.FeatureLint.AcceptanceTests.StepParams;
using UnderTest.FeatureLint.Common;
using UnderTest.FeatureLint.Common.OutputFormatting;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.Common
{
  [UsedImplicitly]
  public class BaseRuleHandler<TRuleType>: BaseRuleHandler
    where TRuleType : ILintRule, new()
  {
    public BaseRuleHandler()
    {
      Rule = new RuleFactory<TRuleType>().GetInstanceWithDefaultConfig();
    }
  }

  public abstract class BaseRuleHandler: FeatureHandler
  {
    [PropertyInjected]
    public FeatureLintCliRunner FeatureLintCliRunner { get; set; }

    protected ILintRule Rule { get; set; }

    protected FeatureLintJsonResult LintResult { get; private set; }

    protected string Content { get; set; }

    [When("I lint the feature file")]
    public void WhenILintTheFeatureFile()
    {
      LintResult = FeatureLintCliRunner.RunFeatureLintCliWithContent(Content);
    }

    // then the `RuleTypeName` error is not given
    // then the `RuleTypeName` error is given
    [Then("the `(.*)` error (is|is not) given")]
    public void TheRuleFails(string ruleName, [IsOrIsNotStepParam] bool ruleExpectedToBeInFindings)
    {
      if (ruleExpectedToBeInFindings)
      {
        LintResult.VerifyHasRuleOfType(Rule.RuleType);
      }
      else
      {
        LintResult.VerifyNoRuleOfType(Rule.RuleType);
      }
    }
  }
}
