using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.AcceptanceTests.Common
{
  public class RuleFactory<TRuleType> where TRuleType: ILintRule, new()
  {
    public TRuleType GetInstanceWithDefaultConfig()
    {
      return new TRuleType
      {
        Config = new DefaultRuleSettingsFactory().GetInstance()
      };
    }
  }
}
