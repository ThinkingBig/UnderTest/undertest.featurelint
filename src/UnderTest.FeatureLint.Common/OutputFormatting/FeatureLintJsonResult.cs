using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.OutputFormatting
{
  public class FeatureLintJsonResult
  {
    public bool Successful { get; set; }

    public IList<FeatureLintResultGroupedByFile> Results { get; set; } = new List<FeatureLintResultGroupedByFile>();

    public List<string> ErrorMessages { get; set; } = new List<string>();

    public List<string> InfoMessages { get; set; } = new List<string>();
  }
}
