namespace UnderTest.FeatureLint.Common.OutputFormatting
{
  public interface IOutputFormatter
  {
    string BuildErrorResult(FeatureLintResult result);

    string BuildSuccessResult();
  }
}
