using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin;
using UnderTest.FeatureLint.Common.Extensions;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintStringLinter
  {
    public FeatureLintStringLinter(IFeatureLintConfig config)
    {
      this.config = config;

      ruleLinters.AddRange(LoadAllRules());
    }

    private readonly IFeatureLintConfig config;

    private List<ILintRule<BaseRuleConfig>> ruleLinters { get; } = new List<ILintRule<BaseRuleConfig>>();

    // todo make this param a type
    public FeatureLintResult Execute(string gherkin)
    {
      var featureLintResult = new FeatureLintResult();
      try
      {
        // todo - make filePath part of the object passed in
        if (!LoadGherkinDetails(gherkin,  "<contents>", out var failureErrors, out var fileDetails))
        {
          featureLintResult.ValidGherkin = false;
          if (failureErrors.Any())
          {
            featureLintResult.Findings.AddRange(failureErrors);
            return featureLintResult;
          }
        }

        featureLintResult.ValidGherkin = true;

        if (fileDetails.Gherkin.Feature.IsMarkedWithTag(config.IgnoreTags))
        {
          featureLintResult.InfoMessages.Add($"Skipping `{fileDetails.FilePath}` as it is marked as ignored.");
          featureLintResult.Ignored = true;
          return featureLintResult;
        }

        var findings = RunLinters(fileDetails);
        if (findings.Any())
        {
          featureLintResult.Findings.AddRange(findings);
        }
      }
      finally
      {
        featureLintResult.UpdateSummary();
      }

      return featureLintResult;
    }

    private bool LoadGherkinDetails(string content, string filePathP, out IList<FeatureLintData> failureErrorsP,
      out FeatureFileDetails fileDetails)
    {
      failureErrorsP = new List<FeatureLintData>();
      fileDetails = null;

      try
      {
        fileDetails = new FeatureFileDetailsFactory
        {
          Contents = content
        }.GetInstance();

        return true;
      }
      catch (CompositeParserException e)
      {
        foreach (var error in e.Errors)
        {
          failureErrorsP.Add(new FeatureLintData
          {
            Type = FeatureLintDataType.InvalidGherkin,
            Line = error.Location.Line,
            Column = error.Location.Column,
            FilePath = filePathP,
            Message =
              $"Expected keyword or gherkin property not found. Make sure you are following proper Gherkin syntax. Details are: {e.Message}"
          });
        }
      }
      catch (ParserException e)
      {
        failureErrorsP.Add(new FeatureLintData
        {
          Type = FeatureLintDataType.InvalidGherkin,
          Line = e.Location.Line,
          Column = e.Location.Column,
          FilePath = filePathP,
          Message =
            $"Expected keyword or gherkin property not found. Make sure you are following proper Gherkin syntax. Details are: {e.Message}"
        });
      }

      return false;
    }

    private static IEnumerable<ILintRule<BaseRuleConfig>> LoadAllRules()
    {
      var iLintRuleType = typeof(ILintRule);
      var rules = iLintRuleType
        .Assembly
        .GetTypes()
        .Where(t =>
          t.IsClass
          &&
          !t.IsAbstract
          &&
          t.GetInterfaces().Contains(iLintRuleType))
        .Select(Activator.CreateInstance)
        .Cast<ILintRule<BaseRuleConfig>>();
      return rules;
    }

    private IList<FeatureLintData> RunLinters(FeatureFileDetails fileDetailsP)
    {
      fileDetailsP.AssertNotNull();
      var result = new List<FeatureLintData>();
      foreach (var linter in ruleLinters)
      {
        linter.Config = config;

        result.AddRange(linter.Run(fileDetailsP));
      }

      return result;
    }
  }
}
