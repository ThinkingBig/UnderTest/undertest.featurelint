using System.Collections.Generic;
using System.IO;
using System.Linq;
using GlobExpressions;

namespace UnderTest.FeatureLint.Common.IO
{
  public static class FileCollector
  {
    public static IEnumerable<string> CollectFilesFromGlobs(string directoryP, IEnumerable<string> globsP)
    {
      var result = new List<string>();

      foreach (var filePath in globsP)
      {
        // if the file exists - add it
        // if this is a directory - find all feature files in the directory
        // otherwise we will glob for the files within the working directory
        if (File.Exists(filePath))
        {
          result.Add(filePath);
          continue;
        }

        if (Directory.Exists(filePath))
        {
          result.AddRange(Glob.Files(filePath, "**/*.feature").ToList());
          continue;
        }

        result.AddRange(Glob.Files(Directory.GetCurrentDirectory(), filePath));
      }

      return result;
    }
  }
}
