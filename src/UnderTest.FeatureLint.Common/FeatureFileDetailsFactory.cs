using System;
using System.IO;
using Gherkin;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureFileDetailsFactory
  {
    public string Contents { get; set; }
    public string FilePath { get; set; }

    public virtual FeatureFileDetails GetInstance()
    {
      if (Contents == null)
      {
        throw new ArgumentNullException(nameof(Contents));
      }

      return new FeatureFileDetails
      {
        Contents = Contents,
        FilePath = FilePath,
        Gherkin = new Parser().Parse(new StringReader(Contents))
      };
    }

    protected virtual string ReadContent(string filePathP)
    {
      return Contents;
    }
  }
}
