using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintResult
  {
    public bool ValidGherkin { get; set; }

    public bool Ignored { get; set; }

    public List<FeatureLintData> Findings { get; } = new List<FeatureLintData>();

    public FeatureLintResultSummary Summary { get; } = new FeatureLintResultSummary();

    public List<string> InfoMessages { get; } = new List<string>();

    public List<string> ErrorMessages { get; } = new List<string>();
  }
}
