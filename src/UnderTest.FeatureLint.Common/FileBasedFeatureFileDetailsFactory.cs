using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common
{
  [PublicAPI]
  public class FileBasedFeatureFileDetailsFactory: FeatureFileDetailsFactory
  {
    public override FeatureFileDetails GetInstance()
    {
      if (string.IsNullOrWhiteSpace(FilePath))
      {
        throw new ArgumentNullException(nameof(FilePath));
      }

      Contents = ReadContent(FilePath);

      return base.GetInstance();
    }

    [ExcludeFromCodeCoverage]
    protected override string ReadContent(string filePathP)
    {
      return File.ReadAllText(filePathP);
    }
  }
}
