using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class EnsureScenarioNamesAreUniqueRule : ILintRule<EnsureScenarioNamesAreUniqueRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public EnsureScenarioNamesAreUniqueRuleConfig RuleConfig => Config.RuleSettings.EnsureScenarioNamesAreUnique;

    public RuleType RuleType { get; } = RuleType.EnsureScenarioNamesAreUniqueRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      var duplicatedScenarios = doc.Feature.Children.GatherAllScenarios()
                                        .GroupBy(x => x.Name, scenario => scenario)
                                        .Where(g => g.Count() > 1)
                                        .ToList();

      result.AddRange(duplicatedScenarios
        .Select(x =>
          new FeatureLintData
          {
            Line = x.ElementAt(0).Location.Line,
            Column = x.ElementAt(0).Location.Column,
            Type = FeatureLintDataType.Error,
            Message = $@"Duplicate scenario with name ""{x.Key}""",
            FilePath = contentsP.FilePath,
            Rule = RuleType
          }));

      return result;
    }
  }
}
