using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.Rules
{
  public interface ILintRule
  {
    int? Order { get; }

    IFeatureLintConfig Config { get; set; }

    IList<FeatureLintData> Run(FeatureFileDetails contentsP);

    RuleType RuleType { get; }
  }

  public interface ILintRule<out TConfigType>: ILintRule
    where TConfigType: BaseRuleConfig
  {
    TConfigType RuleConfig { get; }
  }
}
