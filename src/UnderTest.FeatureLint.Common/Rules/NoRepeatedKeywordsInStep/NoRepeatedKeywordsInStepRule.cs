using System.Collections.Generic;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoRepeatedKeywordsInStepRule : ILintRule<NoRepeatedKeywordsInStepRuleConfig>
  {
    private List<FeatureLintData> result;
    private FeatureFileDetails contents;
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public NoRepeatedKeywordsInStepRuleConfig RuleConfig => Config.RuleSettings.NoRepeatedKeywordsInStep;

    public RuleType RuleType { get; } = RuleType.NoRepeatedKeywordsInStepRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contents = contentsP;
      result = new List<FeatureLintData>();
      contents.AssertNotNull();

      if (!RuleConfig.Enabled || contents.Gherkin.Feature == null)
      {
        return result;
      }

      CheckScenariosForRepeatKeywords();

      return result;
    }

    private void CheckScenariosForRepeatKeywords()
    {
      foreach (var scenario in contents.Gherkin.Feature.Children)
      {
        switch (scenario)
        {
          case StepsContainer stepsContainer:
          {
            CheckStepsForRepeatKeywords(stepsContainer);
            break;
          }
        }
      }
    }

    private void CheckStepsForRepeatKeywords(StepsContainer stepsContainer)
    {
      foreach (var step in stepsContainer.Steps)
      {
        if (StepContainsRepeatedKeyword(step))
        {
          result.Add(new FeatureLintData
          {
            Line = step.Location.Line,
            Column = step.Location.Column,
            FilePath = contents.FilePath,
            Type = FeatureLintDataType.Warning,
            Message = $"There appears to be a repeated keyword in the step `{step.Keyword} {step.Text}`",
            Rule = RuleType
          });
        }
      }
    }

    private bool StepContainsRepeatedKeyword(Step stepP) => stepP.Text.Trim().ToLowerInvariant().StartsWith(stepP.Keyword.Trim().ToLowerInvariant());
  }
}
