﻿using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class FeatureMustHaveDescriptionRule: ILintRule<FeatureMustHaveDescriptionRuleConfig>
  {
    public int? Order => null;
    public IFeatureLintConfig Config { get; set; }
    public FeatureMustHaveDescriptionRuleConfig RuleConfig => Config.RuleSettings.FeatureMustHaveDescription;
    public RuleType RuleType { get; } = RuleType.FeatureMustHaveDescriptionRule;

    public const string NoFeatureDescriptionMessage = "Feature must have a description.";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      if (string.IsNullOrWhiteSpace(doc.Feature.Description))
      {
        result.Add(new FeatureLintData
        {
          Line = 1,
          Column = 0,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = NoFeatureDescriptionMessage,
          Rule = RuleType
        });
      }

      return result;
    }
  }
}
