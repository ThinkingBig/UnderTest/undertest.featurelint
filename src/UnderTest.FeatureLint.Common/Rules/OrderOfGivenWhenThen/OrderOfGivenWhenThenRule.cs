using System.Collections.Generic;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class OrderOfGivenWhenThenRule : ILintRule<OrderOfGivenWhenThenRuleConfig>
  {
    private const string WhenKeyword = "When";
    private const string GivenKeyword = "Given";
    private const string ThenKeyword = "Then";

    private List<FeatureLintData> result;
    private FeatureFileDetails contents;
    public int? Order { get; } = null;
    public IFeatureLintConfig Config { get; set; }
    public OrderOfGivenWhenThenRuleConfig RuleConfig => Config.RuleSettings.OrderOfGivenWhenThen;
    public RuleType RuleType { get; } = RuleType.OrderOfGivenWhenThenRule;

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contents = contentsP;
      contents.AssertNotNull();
      result = new List<FeatureLintData>();

      if (!RuleConfig.Enabled)
      {
        return result;
      }

      if (contents.Gherkin.Feature == null)
      {
        return result;
      }

      CheckEachScenarioForGivenWhenThenOrder();

      return result;
    }

    private void CheckEachScenarioForGivenWhenThenOrder()
    {
      foreach (var scenario in contents.Gherkin.Feature.Children.GatherAllStepsContainers())
      {
        CheckScenarioGivenWhenOrder(scenario);
      }
    }

    private void CheckScenarioGivenWhenOrder(IHasSteps scenarioP)
    {
      var seenGiven = false;
      var seenWhen = false;
      var seenThen = false;
      var lastKeyword = string.Empty;
      foreach (var step in scenarioP.Steps)
      {
        var keyword = step.Keyword.Trim();
        switch (keyword)
        {
          case GivenKeyword:
          case "And" when lastKeyword == GivenKeyword:
          {
            lastKeyword = GivenKeyword;
            seenGiven = true;
            if (seenThen || seenWhen)
            {
              result.Add(MapToFeatureLintData(contents, step, GivenKeyword));
            }

            break;
          }

          case WhenKeyword:
          case "And" when lastKeyword == WhenKeyword:
          {
            lastKeyword = WhenKeyword;
            seenWhen = true;
            if (seenThen)
            {
              result.Add(MapToFeatureLintData(contents, step, WhenKeyword));
            }

            break;
          }

          case ThenKeyword:
          case "And" when lastKeyword == ThenKeyword:
          {
            lastKeyword = ThenKeyword;
            seenThen = true;
            if (!seenGiven && !seenWhen)
            {
              result.Add(MapToFeatureLintData(contents, step, ThenKeyword));
            }

            break;
          }
        }
      }
    }

    private FeatureLintData MapToFeatureLintData(FeatureFileDetails contentsP, IHasLocation step, string keyword)
    {
      return new FeatureLintData
      {
        Line = step.Location.Line,
        Column = step.Location.Column,
        FilePath = contentsP.FilePath,
        Message = $"{keyword} out of order. Given, When and Then must be used in order",
        Type = FeatureLintDataType.Error,
        Rule = RuleType
      };
    }
  }
}
