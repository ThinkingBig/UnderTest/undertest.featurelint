namespace UnderTest.FeatureLint.Common.Rules
{
  public interface IRuleConfig
  {
    bool Enabled { get; set; }
  }
}
