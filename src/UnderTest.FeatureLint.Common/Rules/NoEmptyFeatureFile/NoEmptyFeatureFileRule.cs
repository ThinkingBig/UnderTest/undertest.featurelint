using System.Collections.Generic;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoEmptyFeatureFileRule : ILintRule<NoEmptyFeatureFileRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public NoEmptyFeatureFileRuleConfig RuleConfig => Config.RuleSettings.NoEmptyFeatureFile;

    public RuleType RuleType { get; } = RuleType.NoEmptyFeatureFileRule;

    public static string NoFeatureTitleMessage = "Feature file empty";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        result.Add(new FeatureLintData
        {
          Line = 1,
          Column = 0,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = NoFeatureTitleMessage,
          Rule = RuleType
        });
      }

      return result;
    }
  }
}
