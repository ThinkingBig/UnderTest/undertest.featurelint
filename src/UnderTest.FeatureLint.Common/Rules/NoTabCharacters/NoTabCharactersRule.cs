﻿using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  [PublicAPI]
  public class NoTabCharactersRule: ILintRule<NoTabCharactersRuleConfig>
  {
    public int? Order { get; }
    public IFeatureLintConfig Config { get; set; }

    public NoTabCharactersRuleConfig RuleConfig => Config.RuleSettings.NoTabCharacters;

    public RuleType RuleType { get; } = RuleType.NoTabCharactersRule;

    public IList<FeatureLintData> Run([NotNull]FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(
        from scenario in doc.Feature.Children.GatherAllStepsContainers()
        from step in scenario.Steps
        where StepContainsTabCharacter(step)
        select new FeatureLintData
        {
          Line = step.Location.Line,
          Column = step.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Warning,
          Message = $"Tab character found on line: {step.Location.Line} column: {step.Location.Column}",
          Rule = RuleType
        });

      return result;
    }

    protected virtual bool StepContainsTabCharacter(Step stepP)
    {
      return stepP.Text.Contains('\t');
    }
  }
}
