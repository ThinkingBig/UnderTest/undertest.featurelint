using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class UnmappedExampleElementsRule: ILintRule<UnmappedExampleElementsRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public UnmappedExampleElementsRuleConfig RuleConfig => Config.RuleSettings.UnmappedExampleElements;

    public RuleType RuleType { get; } = RuleType.UnmappedExampleElementsRule;

    private static string ExampleHeadersShouldHaveMatchingParametersMessage = "A header in this example table does not have a matching parameter in the scenario.";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      var scenarios = doc.Feature.Children.GatherAllScenarios();

      result.AddRange(from scenario in scenarios
      from example in scenario.Examples
      let exampleHeaderElements = example.TableHeader?.Cells ?? new TableCell[0]
      from header in exampleHeaderElements
      where !ParameterInScenario(header, scenario)
      select new FeatureLintData
      {
        Line = example.TableHeader.Location.Line,
        Column = example.TableHeader.Location.Column,
        FilePath = contentsP.FilePath,
        Type = FeatureLintDataType.Error,
        Message = ExampleHeadersShouldHaveMatchingParametersMessage,
        Rule = RuleType
      });
      return result;
    }

    public static bool ParameterInScenario(TableCell headerP, Scenario scenarioP )
    {
      var parameterFound = false;

      foreach (var step in scenarioP.Steps)
      {
        if (Regex.IsMatch(step.Text, $"\\b{headerP.Value}\\b"))
        {
          parameterFound = true;
        }

        if (step.Argument?.GetType() != typeof(DataTable)) continue;
        var dataTable = (DataTable)step.Argument;

        foreach (var row in dataTable.Rows)
        {
          foreach (var cell in row.Cells)
          {
            if (Regex.IsMatch(cell.Value, headerP.Value))
            {
              parameterFound = true;
            }
          }
        }
      }

      return parameterFound;
    }
  }
}
