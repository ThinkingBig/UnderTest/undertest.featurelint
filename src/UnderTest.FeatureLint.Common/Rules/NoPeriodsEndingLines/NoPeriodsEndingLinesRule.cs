using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoPeriodsEndingLinesRule : ILintRule<NoPeriodsEndingLinesRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public NoPeriodsEndingLinesRuleConfig RuleConfig => Config.RuleSettings.NoPeriodsEndingLines;
    public RuleType RuleType { get; } = RuleType.NoPeriodsEndingLinesRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      if (EndsWithPeriod(doc.Feature.Name))
      {
        result.Add(BuildResultItem(contentsP, doc.Feature, "Feature name"));
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        if (EndsWithPeriod(scenario.Name))
        {
          result.Add(BuildResultItem(contentsP, scenario, "Scenario name"));
        }

        result.AddRange(from
                          step
                          in scenario.Steps
                        where
                          EndsWithPeriod(step.Text)
                        select
                          BuildResultItem(contentsP, step, "Step"));
      }

      return result;
    }

    private FeatureLintData BuildResultItem(FeatureFileDetails fileContents, IHasLocation locationContext, string contextName)
    {
      return new FeatureLintData
      {
        Line = locationContext.Location.Line,
        Column = locationContext.Location.Column,
        FilePath = fileContents.FilePath,
        Message = $"{contextName} should not end in a period",
        Type = FeatureLintDataType.Warning,
        Rule = RuleType
      };
    }

    private static bool EndsWithPeriod(string input)
    {
      return input.EndsWith(".");
    }
  }
}
