using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ExampleTablesShouldUseLowerKebabCaseRule: ILintRule<ExampleTablesShouldUseLowerKebabCaseRuleConfig>
  {
    public int? Order => null;

    public IFeatureLintConfig Config { get; set; }

    public ExampleTablesShouldUseLowerKebabCaseRuleConfig RuleConfig =>
      Config.RuleSettings.ExampleTablesShouldUseLowerKebabCase;

    public RuleType RuleType { get; } = RuleType.ExampleTablesShouldUseLowerKebabCaseRule;

    private const string ExampleTableHeaderNotLowerKebabCaseMessage = "Example Headers must be lower Kebab Case. Ex: `| order-summary |`";

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      result.AddRange(from scenario in doc.Feature.Children.GatherAllScenarios()
        from example in scenario.Examples
        from header in example.TableHeader?.Cells ?? new TableCell[0]
        where header.Value != ToLowerKebabCase(header.Value)
        select new FeatureLintData
        {
          Line = header.Location.Line,
          Column = header.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = ExampleTableHeaderNotLowerKebabCaseMessage,
          Rule = RuleType
        });

      return result;
    }

    public static string ToLowerKebabCase(string s)
    {
      var lowerCaseKebabString = s.Replace(' ', '-').ToLower();

      return lowerCaseKebabString;
    }
  }
}
