using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class BadWordsRuleConfig: BaseRuleConfig
  {
    public List<string> VaguePhrases { get; set; } = new List<string> {"or", "and", "and/or", "if", "can", "may", "might"};

    public List<string> Untestable { get; set; } = new List<string> {"need", "needs", "require", "requires", "wants", "want"};
  }
}
