
namespace UnderTest.FeatureLint.Common.Rules
{
  public class BadPhrase
  {
    public BadPhrase(string phraseP, BadPhraseType typeP)
    {
      Phrase = phraseP;
      Type = typeP;
    }

    public string Phrase { get; }

    public BadPhraseType Type { get; }
  }
}
