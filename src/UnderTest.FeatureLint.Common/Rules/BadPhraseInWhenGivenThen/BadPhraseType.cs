
namespace UnderTest.FeatureLint.Common.Rules
{
  public enum BadPhraseType
  {
    Vague = 1,
    Untestable = 2
  }
}
