﻿using System.Collections.Generic;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class OnlyOneWhenPerScenarioRuleConfig: BaseRuleConfig
  {
    public List<string> WhiteListedWords { get; set; } = new List<string>();
  }
}
