using System;
using System.Collections.Generic;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ScenarioNameRequiredRule : ILintRule<ScenarioNameRequiredRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public ScenarioNameRequiredRuleConfig RuleConfig => Config.RuleSettings.ScenarioNameRequired;
    public RuleType RuleType { get; } = RuleType.ScenarioNameRequiredRule;

    public static string NoScenarioNameMessage = "Scenario must have a non-empty name";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers())
      {
        if (string.IsNullOrWhiteSpace(scenario.Name))
        {
          result.Add(new FeatureLintData
          {
            Line = scenario.Location.Line,
            Column = scenario.Location.Column,
            FilePath = contentsP.FilePath,
            Type = FeatureLintDataType.Error,
            Message = NoScenarioNameMessage,
            Rule = RuleType
          });
        }
      }

      return result;
    }
  }
}
