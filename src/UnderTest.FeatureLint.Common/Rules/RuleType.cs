namespace UnderTest.FeatureLint.Common.Rules
{
  public enum RuleType
  {
    NoEmptyFeatureFileRule = 1,
    BadPhraseInWhenGiveThenRule = 2,
    NoDuplicateWhenGivenThenRule = 3,
    ScenarioNameRequiredRule = 4,
    DataTableHeadersUsePascalCaseRule = 5,
    UnmappedExampleElementsRule = 6,
    ExampleTablesShouldUseLowerKebabCaseRule = 7,
    NoEmptyScenarioRule = 8,
    NoTagsOnExampleTablesRule = 9,
    ExampleTableHeadersAreUniqueRule = 10,
    EnsureFeatureNamesAreUniqueRule = 11,
    EnsureScenarioNamesAreUniqueRule = 12,
    FeatureNameRequiredRule = 13,
    NoDuplicateTagsRule = 14,
    NoPeriodsEndingLinesRule = 15,
    NoThenInBackgroundsRule = 16,
    NoUpperCaseWordsRule = 17,
    OnlyOneWhenPerScenarioRule = 18,
    OrderOfGivenWhenThenRule = 19,
    ScenarioOutlineShouldHaveAnExampleTableRule = 20,
    ScenarioOutlineExamplesShouldHaveANameRule = 21,
    FeatureMustHaveDescriptionRule = 22,
    ExampleTableHeadersDoNotIncludeAngleBracketsRule = 23,
    NoTabCharactersRule = 24,
    EnsureNoEmptyStepsRule = 25,
    NoRepeatedKeywordsInStepRule = 26,
    TagCanBePromotedToTheFeatureLevelRuleConfig = 27,
  }
}
