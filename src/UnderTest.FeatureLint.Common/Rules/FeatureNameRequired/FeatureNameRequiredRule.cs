using System.Collections.Generic;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class FeatureNameRequiredRule : ILintRule<FeatureNameRequiredRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public FeatureNameRequiredRuleConfig RuleConfig => Config.RuleSettings.FeatureNameRequiredRule;
    public RuleType RuleType { get; } = RuleType.FeatureNameRequiredRule;

    public static string NoFeatureTitleMessage = "Feature must have a non-empty name";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      if (string.IsNullOrWhiteSpace(doc.Feature.Name))
      {
        result.Add(new FeatureLintData
        {
          Line = 1,
          Column = 0,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Message = NoFeatureTitleMessage,
          Rule = RuleType
        });
      }

      return result;
    }
  }
}
