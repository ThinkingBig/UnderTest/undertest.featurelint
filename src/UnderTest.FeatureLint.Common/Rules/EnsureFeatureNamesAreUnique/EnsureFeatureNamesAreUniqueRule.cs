using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class EnsureFeatureNamesAreUniqueRule : ILintRule<EnsureFeatureNamesAreUniqueRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public EnsureFeatureNamesAreUniqueRuleConfig RuleConfig => Config.RuleSettings.EnsureFeatureNamesAreUnique;

    public RuleType RuleType { get; } = RuleType.EnsureFeatureNamesAreUniqueRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      if (contentsP.Gherkin.Feature == null)
      {
        return result;
      }

      var feature = contentsP.Gherkin.Feature;
      var duplicatedScenarios = contentsP.OtherValidFiles
                                  .Where(x => x.Gherkin.Feature != null && string.Equals(
                                    x.Gherkin.Feature.Name,
                                    feature.Name,
                                    StringComparison.InvariantCultureIgnoreCase))
                                  .ToList();

      result.AddRange(duplicatedScenarios
        .Select(x =>
          new FeatureLintData
          {
            Line = feature.Location.Line,
            Column = feature.Location.Column,
            Type = FeatureLintDataType.Error,
            Message = $@"Duplicate feature name with name ""{feature.Name}""",
            FilePath = contentsP.FilePath,
            Rule = RuleType
          }));

      return result;
    }
  }
}
