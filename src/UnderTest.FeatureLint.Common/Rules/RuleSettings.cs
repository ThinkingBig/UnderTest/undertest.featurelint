namespace UnderTest.FeatureLint.Common.Rules
{
  public class RuleSettings
  {
    public BadWordsRuleConfig BadPhrases { get; } = new BadWordsRuleConfig();

    public ScenarioNameRequiredRuleConfig ScenarioNameRequired { get; } = new ScenarioNameRequiredRuleConfig();

    public NoDuplicateWhenGivenThenRuleConfig NoDuplicateWhenGivenThenRule { get; } = new NoDuplicateWhenGivenThenRuleConfig();

    public ExampleTableHeadersAreUniqueRuleConfig ExampleTableHeadersAreUniqueRule { get; } = new ExampleTableHeadersAreUniqueRuleConfig();

    public ExampleTableHeadersDoNotIncludeAngleBracketsRuleConfig ExampleTableHeadersDoNotIncludeAngleBracketsRule { get; } = new ExampleTableHeadersDoNotIncludeAngleBracketsRuleConfig();

    public DataTableHeadersUsePascalCaseRuleConfig DataTableHeadersUsePascalCase { get; } =
      new DataTableHeadersUsePascalCaseRuleConfig();

    public EnsureFeatureNamesAreUniqueRuleConfig EnsureFeatureNamesAreUnique { get; } = new EnsureFeatureNamesAreUniqueRuleConfig();

    public NoEmptyStepsRuleConfig NoEmptySteps { get; } = new NoEmptyStepsRuleConfig();

    public EnsureScenarioNamesAreUniqueRuleConfig EnsureScenarioNamesAreUnique { get; } = new EnsureScenarioNamesAreUniqueRuleConfig();

    public ExampleTablesShouldUseLowerKebabCaseRuleConfig ExampleTablesShouldUseLowerKebabCase { get; }
      = new ExampleTablesShouldUseLowerKebabCaseRuleConfig();

    public FeatureNameRequiredRuleConfig FeatureNameRequiredRule { get; } = new FeatureNameRequiredRuleConfig();

    public FeatureMustHaveDescriptionRuleConfig FeatureMustHaveDescription { get; } = new FeatureMustHaveDescriptionRuleConfig();

    public NoDuplicateTagsRuleConfig NoDuplicateTagsRule { get; } = new NoDuplicateTagsRuleConfig();

    public NoEmptyFeatureFileRuleConfig NoEmptyFeatureFile { get; } = new NoEmptyFeatureFileRuleConfig();

    public NoEmptyScenarioRuleConfig NoEmptyScenario { get; } = new NoEmptyScenarioRuleConfig();

    public NoPeriodsEndingLinesRuleConfig NoPeriodsEndingLines { get; } = new NoPeriodsEndingLinesRuleConfig();

    public NoTagsOnExampleTablesRuleConfig NoTagsOnExampleTables { get; } = new NoTagsOnExampleTablesRuleConfig();

    public NoThenInBackgroundsRuleConfig NoThenInBackgrounds { get; } = new NoThenInBackgroundsRuleConfig();
    public NoUpperCaseWordsRuleConfig NoUpperCaseWords { get; } = new NoUpperCaseWordsRuleConfig();

    public OnlyOneWhenPerScenarioRuleConfig OnlyOneWhenPerScenario { get; } = new OnlyOneWhenPerScenarioRuleConfig();

    public OrderOfGivenWhenThenRuleConfig OrderOfGivenWhenThen { get; } = new OrderOfGivenWhenThenRuleConfig();

    public ScenarioOutlineShouldHaveAnExampleTableRuleConfig ScenarioOutlineShouldHaveAnExampleTable { get; }
      = new ScenarioOutlineShouldHaveAnExampleTableRuleConfig();

    public ScenarioOutlineExamplesShouldHaveANameRuleConfig ScenarioOutlineExamplesShouldHaveAName { get; }
      = new ScenarioOutlineExamplesShouldHaveANameRuleConfig();

    public UnmappedExampleElementsRuleConfig UnmappedExampleElements { get; } = new UnmappedExampleElementsRuleConfig();
    public NoTabCharactersRuleConfig NoTabCharacters { get; } = new NoTabCharactersRuleConfig();

    public NoRepeatedKeywordsInStepRuleConfig NoRepeatedKeywordsInStep { get; set; } = new NoRepeatedKeywordsInStepRuleConfig();

    public TagCanBePromotedToTheFeatureLevelRuleConfig TagCanBePromotedToTheFeatureLevel { get; set; } = new TagCanBePromotedToTheFeatureLevelRuleConfig();
  }
}
