using System.Collections.Generic;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class ScenarioOutlineExamplesShouldHaveANameRule : ILintRule<ScenarioOutlineExamplesShouldHaveANameRuleConfig>
  {
    public static string ScenarioOutlineExamplesShouldHaveANameMessage = "Examples should have a name";

    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }

    public ScenarioOutlineExamplesShouldHaveANameRuleConfig RuleConfig =>
      Config.RuleSettings.ScenarioOutlineExamplesShouldHaveAName;

    public RuleType RuleType { get; } = RuleType.ScenarioOutlineExamplesShouldHaveANameRule;

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllScenarios())
      {
        foreach (var example in scenario.Examples)
        {
          if (string.IsNullOrWhiteSpace(example.Name))
          {
            result.Add(new FeatureLintData
            {
              Line = example.Location.Line,
              Column = example.Location.Column,
              FilePath = contentsP.FilePath,
              Type = FeatureLintDataType.Error,
              Message = ScenarioOutlineExamplesShouldHaveANameMessage,
              Rule = RuleType
            });
          }
        }
      }

      return result;
    }
  }
}
