using System;
using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoEmptyScenarioRule : ILintRule<NoEmptyScenarioRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public NoEmptyScenarioRuleConfig RuleConfig => Config.RuleSettings.NoEmptyScenario;

    public RuleType RuleType { get; } = RuleType.NoEmptyScenarioRule;

    public static string NoEmptyBackgroundMessage = "Background is empty, add steps or remove this background";
    public static string NoEmptyScenarioMessage = "Scenario is empty, add steps or remove this scenario";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!Config.RuleSettings.NoEmptyFeatureFile.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      foreach (var scenario in doc.Feature.Children.GatherAllStepsContainers(includeBackground: true))
      {
        if (scenario.Steps.Any())
        {
          continue;
        }

        var lintData = new FeatureLintData
        {
          Line = scenario.Location.Line,
          Column = scenario.Location.Column,
          FilePath = contentsP.FilePath,
          Type = FeatureLintDataType.Error,
          Rule = RuleType,
          Message = scenario is Background ? NoEmptyBackgroundMessage : NoEmptyScenarioMessage
        };

        result.Add(lintData);
      }

      return result;
    }
  }
}
