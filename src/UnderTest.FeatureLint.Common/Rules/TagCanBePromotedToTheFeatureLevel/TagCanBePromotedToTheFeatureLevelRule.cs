using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class TagCanBePromotedToTheFeatureLevelRule : ILintRule<TagCanBePromotedToTheFeatureLevelRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public TagCanBePromotedToTheFeatureLevelRuleConfig RuleConfig => Config.RuleSettings.TagCanBePromotedToTheFeatureLevel;

    public RuleType RuleType { get; } = RuleType.TagCanBePromotedToTheFeatureLevelRuleConfig;

    private readonly IList<ScenarioTagDetails> firstScenarioTagsOnEveryScenario = new List<ScenarioTagDetails>();

    private bool isFirstScenario;

    public IList<FeatureLintData> Run(FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      if (!RuleConfig.Enabled)
      {
        return new List<FeatureLintData>();
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return new List<FeatureLintData>();
      }

      isFirstScenario = true;
      firstScenarioTagsOnEveryScenario.Clear();
      var children = doc.Feature.Children;
      ProcessChildScenarios(contentsP, children);

      return BuildResult(contentsP);
    }

    private void ProcessChildScenarios(FeatureFileDetails contentsP, IEnumerable<IHasLocation> children)
    {
      foreach (var child in children)
      {
        switch (child)
        {
          case Scenario scenario:
            RemoveNonDuplicateTags(contentsP, scenario);
            break;
          case Rule rule:
            ProcessChildScenarios(contentsP, rule.Children);
            break;
        }
      }
    }

    private void RemoveNonDuplicateTags(FeatureFileDetails contentsP, Scenario scenario)
    {
      if (isFirstScenario)
      {
        foreach (var tag in scenario.Tags)
        {
          firstScenarioTagsOnEveryScenario.Add(new ScenarioTagDetails
          {
            Scenario = scenario,
            Tag = tag.Name
          });
        }

        isFirstScenario = false;
      }
      else
      {
        for (var index = firstScenarioTagsOnEveryScenario.Count-1; index > 0; index--)
        {
          if (scenario.Tags.Any(tag => tag.Name == firstScenarioTagsOnEveryScenario[index].Tag))
          {
            firstScenarioTagsOnEveryScenario.RemoveAt(index);
          }
        }
      }
    }

    private List<FeatureLintData> BuildResult(FeatureFileDetails contentsP)
    {
      var result = new List<FeatureLintData>();

      foreach (var tag in firstScenarioTagsOnEveryScenario)
      {
        result.Add(new FeatureLintData
        {
          Line = tag.Scenario.Location.Line,
          Column = tag.Scenario.Location.Column,
          Type = FeatureLintDataType.Warning,
          FilePath = contentsP.FilePath,
          Message = $"The tag `{tag.Tag}` is marked on all scenarios and can be moved to the `Feature:` declaration",
          Rule = RuleType
        });
      }

      return result;
    }

    private class ScenarioTagDetails
    {
      public Scenario Scenario { get; set; }
      public string Tag { get; set; }
    }
  }
}
