using System.Collections.Generic;
using System.Linq;
using Gherkin.Ast;
using JetBrains.Annotations;

namespace UnderTest.FeatureLint.Common.Rules
{
  public class NoThenInBackgroundsRule : ILintRule<NoThenInBackgroundsRuleConfig>
  {
    public int? Order { get; } = null;

    public IFeatureLintConfig Config { get; set; }
    public NoThenInBackgroundsRuleConfig RuleConfig => Config.RuleSettings.NoThenInBackgrounds;

    public RuleType RuleType { get; } = RuleType.NoThenInBackgroundsRule;

    public static string NoThenInBackgroundsMessage = "Background should not contain then statements";

    public IList<FeatureLintData> Run([NotNull] FeatureFileDetails contentsP)
    {
      contentsP.AssertNotNull();

      var result = new List<FeatureLintData>();
      if (!RuleConfig.Enabled)
      {
        return result;
      }

      var doc = contentsP.Gherkin;
      if (doc.Feature == null)
      {
        return result;
      }

      const string thenKeyword = "then";
      foreach (var scenario in doc.Feature.Children)
      {
        switch (scenario)
        {
          case Background background:
            result.AddRange(from step in background.Steps
                            where step.Keyword.ToLower().Contains(thenKeyword)
                            select new FeatureLintData
                            {
                              Line = step.Location.Line,
                              Column = step.Location.Column,
                              FilePath = contentsP.FilePath,
                              Type = FeatureLintDataType.Error,
                              Message = NoThenInBackgroundsMessage,
                              Rule = RuleType
                            });
            break;
        }
      }

      return result;
    }
  }
}
