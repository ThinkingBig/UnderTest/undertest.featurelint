using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace Gherkin.Ast
{
  public static class EnumerableExtensions
  {
    public static IList<Scenario> GatherAllScenarios(this IEnumerable<IHasLocation> instance)
    {
      var result = new List<Scenario>();

      foreach (var child in instance)
      {
        switch (child)
        {
          case Scenario scenario:
            result.Add(scenario);
            break;
          case Rule rule:
            var children = rule.Children.OfType<Scenario>().ToList();
            if (children.Any())
            {
              result.AddRange(children);
            }
            break;
        }
      }

      return result;
    }

    public static IList<StepsContainer> GatherAllStepsContainers(this IEnumerable<IHasLocation> instance, bool includeBackground = false)
    {
      var result = new List<StepsContainer>();

      foreach (var child in instance)
      {
        switch (child)
        {
          case Background background:
            if (includeBackground)
            {
              result.Add(background);
            }
            break;
          case Scenario scenario:
            result.Add(scenario);
            break;
          case Rule rule:
            var children = GatherAllStepsContainers(rule.Children);
            if (children.Any())
            {
              result.AddRange(children);
            }
            break;
        }
      }

      return result;
    }

    public static IList<StepsContainer> GatherAllScenariosIncludingBackgrounds(this IEnumerable<IHasLocation> instance)
    {
      return instance.GatherAllStepsContainers(true);
    }
  }
}
