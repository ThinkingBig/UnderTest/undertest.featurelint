using System.Text.RegularExpressions;

// ReSharper disable once CheckNamespace
namespace System
{
  public static class StringExtensions
  {
    public static string RemoveQuotedText(this string instance)
    {
      if (instance == null)
      {
        return null;
      }

      var doubleQuoteRegex = new Regex("\"([^\"]*)\"");
      var singleQuoteRegex = new Regex("'([^\"]*)'");

      var result = doubleQuoteRegex.Replace(instance,string.Empty);

      return singleQuoteRegex.Replace(result, string.Empty);
    }

    public static string ToCamelCase(this string instance)
    {
      if (string.IsNullOrEmpty(instance) || instance.Length <= 1)
      {
        return instance?.ToLowerInvariant() ?? string.Empty;
      }

      return char.ToLowerInvariant(instance[0]) + instance.Substring(1);
    }
  }
}
