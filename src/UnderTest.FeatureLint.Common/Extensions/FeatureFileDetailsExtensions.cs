﻿using System;

namespace UnderTest.FeatureLint.Common
{
  public static class FeatureFileDetailsExtensions
  {
    public static void AssertNotNull(this FeatureFileDetails contentsP)
    {
      if (contentsP == null)
      {
        throw new ArgumentNullException(nameof(contentsP));
      }
    }
  }
}
