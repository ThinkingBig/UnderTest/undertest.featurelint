﻿using System.Collections.Generic;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common
{
  public class FeatureLintConfig : IFeatureLintConfig
  {
    public IList<string> FilePaths { get; set; }

    public IList<string> IgnoreTags { get; set; } = new[] {"featurelint-ignore"};

    public string ExampleDataTagName { get; set; } = "@ExampleData";

    public RuleSettings RuleSettings { get; } = new RuleSettings();
  }
}
