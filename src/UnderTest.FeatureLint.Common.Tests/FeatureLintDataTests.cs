using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureLintDataTests
  {
    [Test]
    public void Constructor_WhenCalled_SetsDefaults()
    {
      var instance = new FeatureLintData();

      instance.Type.Should().BeEquivalentTo(FeatureLintDataType.Error);
    }
  }
}
