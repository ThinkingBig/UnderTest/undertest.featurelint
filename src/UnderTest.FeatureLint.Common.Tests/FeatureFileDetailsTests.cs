using FluentAssertions;
using NUnit.Framework;

namespace UnderTest.FeatureLint.Common.Tests
{
  public class FeatureFileDetailsTests
  {
    [Test]
    public void GetInstance_PassedNull_ThrowsException()
    {
      var instance = new FeatureFileDetails();

      instance.Gherkin.Should().BeNull();
    }
  }
}
