using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class ExampleTablesShouldUseLowerKebabCaseRuleTests: BaseRuleTests<ExampleTablesShouldUseLowerKebabCaseRule>
  {
    [Test]
    public void Run_WhenPassedExampleTableHeaderLowerKebab_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableIsLowerKebabCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableWithNoRows_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableWithNoRows();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableHeaderNotLowerKebab_ReturnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableNotLowerKebabCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(10);
      result.Column.Should().Be(5);
      result.Message.Should().Be("Example Headers must be lower Kebab Case. Ex: `| order-summary |`");
      result.Rule.Should().Be(RuleType.ExampleTablesShouldUseLowerKebabCaseRule);
    }

    [Test]
    public void WhenPassedNonLowerKebabString_ReturnLowerKebabVersion()
    {
      const string nonLowerKebabString = "Happy Tomato";
      var returnedLowerKebabString = ExampleTablesShouldUseLowerKebabCaseRule.ToLowerKebabCase(nonLowerKebabString);

      returnedLowerKebabString.Should().Be("happy-tomato");
    }

    [Test]
    public void WhenPassedLowerKebabString_ReturnLowerKebabVersion()
    {
      const string nonLowerKebabString = "happy";
      var returnedLowerKebabString = ExampleTablesShouldUseLowerKebabCaseRule.ToLowerKebabCase(nonLowerKebabString);

      returnedLowerKebabString.Should().Be("happy");
    }
  }
}
