using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;
using UnderTest.FeatureLint.Common.Tests.TestFactories;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public abstract class BaseRuleTests<TTypeUnderTest>
    where TTypeUnderTest: ILintRule, new()
  {
    [SetUp]
    public void Setup()
    {
      MetaFactory = new TestableFeatureFileDetailsFactoryFactory();
    }

    protected TestableFeatureFileDetailsFactoryFactory MetaFactory;

    [Test]
    public virtual void WhenCreated_Order_ShouldBeNull()
    {
      var instance = BuildRule();

      instance.Order.Should().BeNull();
    }

    [Test]
    public virtual void WhenCreated_RuleConfig_ShouldBeNull()
    {
      dynamic instance = BuildRule();

      Assert.NotNull(instance.RuleConfig);
    }

    [Test]
    public void Run_WhenPassedNull_ShouldThrow()
    {
      var instance = BuildRule();

      Action act = () => instance.Run(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void Run_WhenPassedSimpleValidFile_ReturnsNoResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.SimpleTestFileWithNoScenarios();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public virtual void Run_WhenPassedEmptyFile_ReturnsResults()
    {
      var instance = BuildRule();
      var contents = MetaFactory.EmptyFile();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedExampleTableWithNoRows_ShouldNotThrowAnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithExampleTableWithNoRows();
      var results = instance.Run(contents);

      results.Count.Should().NotBe(null);
    }

    protected static TTypeUnderTest BuildRule()
    {
      return new TTypeUnderTest
      {
        Config = new DefaultRuleSettingsFactory().GetInstance(),
      };
    }
  }
}
