using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoThensInBackgroundsRuleTests : BaseRuleTests<NoThenInBackgroundsRule>
  {
    [Test]
    public void Run_BackgroundContainsThen_ReturnsAWarning()
    {
      var instance = BuildRule();
      var content = MetaFactory.BackgroundContainsThen();

      var results = instance.Run(content);

      results.Count.Should().Be(1);

      var result = results.First();
      result.Line.Should().Be(7);
      result.Column.Should().Be(3);
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Message.Should().Be(NoThenInBackgroundsRule.NoThenInBackgroundsMessage);
      result.Rule.Should().Be(RuleType.NoThenInBackgroundsRule);
    }
  }
}
