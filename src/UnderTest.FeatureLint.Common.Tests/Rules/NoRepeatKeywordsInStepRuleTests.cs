using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoRepeatKeywordsInStepRuleTests : BaseRuleTests<NoRepeatedKeywordsInStepRule>
  {
    [Test]
    public void Run_FeatureEndsInPeriod_ReturnsAnError()
    {
      var instance = BuildRule();
      var content = MetaFactory.StepWithRepeatKeyword();

      var results = instance.Run(content);
      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccurringAt(result, 6, 3);
    }

    [Test]
    public void Run_ScenarioNameEndsInPeriodOnAndLine_ReturnsAnError()
    {
      var instance = BuildRule();
      var content = MetaFactory.StepWithRepeatKeywordOnAndLine();

      var results = instance.Run(content);
      results.Count.Should().Be(1);
      var result = results.First();

      AssertResultIsRuleOccurringAt(result, 7, 5);
    }

    private static void AssertResultIsRuleOccurringAt(FeatureLintData result, int lineP, int colP)
    {
      result.Line.Should().Be(lineP);
      result.Column.Should().Be(colP);
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Message.Should().StartWith("There appears to be a repeated keyword in the step");
      result.Rule.Should().Be(RuleType.NoRepeatedKeywordsInStepRule);
    }
  }
}
