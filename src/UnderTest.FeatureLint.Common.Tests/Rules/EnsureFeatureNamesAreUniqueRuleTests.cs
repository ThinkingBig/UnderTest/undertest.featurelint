using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class EnsureFeatureNamesAreUniqueRuleTests : BaseRuleTests<EnsureFeatureNamesAreUniqueRule>
  {
    [Test]
    public void Run_WhenPassedMultipleValidFilesWithNoDupNames_ReturnEmptyResult()
    {

      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithSimpleValidSteps();
      contents.OtherValidFiles.Add(MetaFactory.OneScenarioWithSimpleValidSteps("another name"));

      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedDuplicateFeatureName_ReturnDuplicateFeature()
    {

      var instance = BuildRule();
      var contents = MetaFactory.TwoScenariosWithDuplicateNames();
      contents.OtherValidFiles.Add(MetaFactory.OneScenarioWithSimpleValidSteps());

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(2);
      result.Column.Should().Be(1);
      result.Message.Should().Contain("Duplicate feature name");
      result.Rule.Should().Be(RuleType.EnsureFeatureNamesAreUniqueRule);
    }
  }
}
