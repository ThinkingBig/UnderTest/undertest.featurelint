using System.Linq;
using FluentAssertions;
using Gherkin.Ast;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class OnlyOneWhenPerScenarioRuleTests : BaseRuleTests<OnlyOneWhenPerScenarioRule>
  {
    [Test]
    public void Run_OnlyOneGivenPerScenarioRule_ReturnsErrorForEachTags()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithMultipleWhenClauses();

      var results = instance.Run(contents);
      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Line.Should().Be(4);
      result.Column.Should().Be(1);
      result.Message.Should().Be(OnlyOneWhenPerScenarioRule.OnlyOneWhenClauseWarningMessage);
      result.Rule.Should().Be(RuleType.OnlyOneWhenPerScenarioRule);
    }
  }
}
