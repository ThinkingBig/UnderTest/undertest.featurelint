using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class BadPhraseInWhenGiveThenRuleTests: BaseRuleTests<BadPhraseInWhenGivenThenRule>
  {
    [Test]
    public void Run_WhenPassedValidFileWithBadPhraseInQuotes_ReturnsNoBrokenRules()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithBadPhraseInQuotes();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public void Run_WhenPassedValidFileWithBadPhrase_ReturnsBadPhrase()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithBadPhrase();

      var result = instance.Run(contents);

      result.Count.Should().Be(3);

      var first = result.First();
      var second = result.Skip(1).First();
      var third = result.Skip(2).First();

      AssertResultIsRuleOccuringAt(first, 5, "may");
      AssertResultIsRuleOccuringAt(second, 6, "wants");
      AssertResultIsRuleOccuringAt(third, 7, "might");
    }

    [Test]
    public void Run_WhenPassedValidFileWithBadPhraseThatAreTheLastWordOnTheLine_ReturnsBadPhrase()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithBadPhraseAsTheLastWordOnTheLine();

      var result = instance.Run(contents);
      result.Count.Should().Be(1);
      var first = result.First();

      AssertResultIsRuleOccuringAt(first, 7, "and");
    }

    [Test]
    public void Run_WhenPassedInvalidFileWithBadPhraseThatNotLowerCase_ReturnsBadPhrase()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithBadPhraseWhereTheBadPhraseIsUpperCase();

      var result = instance.Run(contents);
      result.Count.Should().Be(1);
      var first = result.First();

      AssertResultIsRuleOccuringAt(first, 7, "and");
    }

    private void AssertResultIsRuleOccuringAt(FeatureLintData result, int lineP, string phraseP)
    {
      result.Type.Should().Be(FeatureLintDataType.Warning);
      result.Line.Should().Be(lineP);
      result.Message.Should().StartWith($"Bad phrase found '{phraseP}' - on line");
      result.Rule.Should().Be(RuleType.BadPhraseInWhenGiveThenRule);
    }
  }
}
