using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class DataTableHeadersUsePascalCaseRuleTests: BaseRuleTests<DataTableHeadersUsePascalCaseRule>
  {
    [Test]
    public void WhenPassedDataTableHeaderNotPascal_ReturnError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithDataTableNotPascalCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();

      result.Type.Should().Be(FeatureLintDataType.Error);
      result.Line.Should().Be(6);
      result.Column.Should().Be(7);
      result.Message.Should().Be("Headers in data tables must be Pascal Case");
      result.Rule.Should().Be(RuleType.DataTableHeadersUsePascalCaseRule);
    }

    [Test]
    public void WhenPassedDataTableHeaderPascal_ReturnNoError()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithDataTableIsPascalCase();
      var results = instance.Run(contents);

      results.Count.Should().Be(0);
    }

    [Test]
    public void WhenPassedNonPascalString_ReturnPascalVersion()
    {
      var nonPascalString = "another cat";
      var returnedPascalString = DataTableHeadersUsePascalCaseRule.ToPascalCase(nonPascalString);

      returnedPascalString.Should().Be("AnotherCat");
    }

    [Test]
    public void WhenPassedPascalString_ReturnPascalVersion()
    {
      var nonPascalString = "Cat";
      var returnedPascalString = DataTableHeadersUsePascalCaseRule.ToPascalCase(nonPascalString);

      returnedPascalString.Should().Be("Cat");
    }
  }
}
