﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;
using UnderTest.FeatureLint.Common.Tests.TestFactories;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class FeatureMustHaveDescriptionRuleTests: BaseRuleTests<FeatureMustHaveDescriptionRule>
  {
    [Test]
    public void Run_WhenPassedFeatureWithNoDescription_ReturnsAnError()
    {
      var instance = BuildRule();
      var contents = TestableFeatureFileDetailsFactoryFactory.OneFeatureWithoutDescription();

      var results = instance.Run(contents);

      results.Count.Should().Be(1);
      var result = results.First();
      result.Line.Should().Be(1);
      result.Column.Should().Be(0);
      result.Message.Should().Be(FeatureMustHaveDescriptionRule.NoFeatureDescriptionMessage);
      result.Rule.Should().Be(RuleType.FeatureMustHaveDescriptionRule);
    }
  }
}
