using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Rules
{
  public class NoTagsOnExampleTablesRuleTests : BaseRuleTests<NoTagsOnExampleTablesRule>
  {
    [Test]
    public void Run_NoTagsOnExampleTablesRuleTests_ReturnsErrorForEachTags()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithWithTagsOnExamples();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(9);
      found.Column.Should().Be(1);
      found.Message.Should()
        .Be(NoTagsOnExampleTablesRule.ExampleTableShouldNotHaveTagsMessage);
      found.Rule.Should().Be(RuleType.NoTagsOnExampleTablesRule);
    }

    [Test]
    public void Run_ExternalTagsOnExampleTablesRuleTests_ReturnsErrorForEachTags()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithWithExampleDataTagOnExamples();

      var result = instance.Run(contents);

      result.Count.Should().Be(0);
    }

    [Test]
    public void Run_ExternalTagAndAnotherOnExampleTablesRuleTests_ReturnsErrorForEachTags()
    {
      var instance = BuildRule();
      var contents = MetaFactory.OneScenarioWithWithExampleDataTagAndAnotherOnExamples();

      var result = instance.Run(contents);

      result.Count.Should().Be(1);
      var found = result.First();
      found.Type.Should().Be(FeatureLintDataType.Warning);
      found.Line.Should().Be(9);
      found.Column.Should().Be(1);
      found.Message.Should()
        .Be(NoTagsOnExampleTablesRule.ExampleTableShouldNotHaveTagsMessage);
      found.Rule.Should().Be(RuleType.NoTagsOnExampleTablesRule);
    }
  }
}
