using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.Extensions;

namespace UnderTest.FeatureLint.Common.Tests.Extensions
{
  public class FeatureLintResultExtensionsTests
  {
    [Test]
    public void UpdateSummary_WhenEmptyFindings_SummaryIsAllZeros()
    {
      var instance = new FeatureLintResult();

      instance.UpdateSummary();

      instance.Summary.Errors.Should().Be(0);
      instance.Summary.Warnings.Should().Be(0);
      instance.Summary.TotalFindings.Should().Be(0);
    }

    [Test]
    [TestCase(1, 1, 2)]
    [TestCase(0, 1, 1)]
    [TestCase(1, 0, 1)]
    public void UpdateSummary_WhenAndErrorAndWarnings_TotalsShouldAddUp(int numErrors, int numWarnings, int total)
    {
      var instance = new FeatureLintResult();
      for (var i = 0; i < numErrors; i++)
      {
        instance.Findings.Add(new FeatureLintData { Type = FeatureLintDataType.Error });
      }
      for (var i = 0; i < numWarnings; i++)
      {
        instance.Findings.Add(new FeatureLintData { Type = FeatureLintDataType.Warning });
      }

      instance.UpdateSummary();

      instance.Summary.Errors.Should().Be(numErrors);
      instance.Summary.Warnings.Should().Be(numWarnings);
      instance.Summary.TotalFindings.Should().Be(total);
    }
  }
}
