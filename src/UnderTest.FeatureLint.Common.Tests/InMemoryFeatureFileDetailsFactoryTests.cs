namespace UnderTest.FeatureLint.Common.Tests
{
  using FluentAssertions;

  using NUnit.Framework;

  public class InMemoryFeatureFileDetailsFactoryTests
  {
    [Test]
    public void GetInstance_PassedContent_CreatesFeatureForThatContent()
    {
      var instance = new FeatureFileDetailsFactory
                       {
                         Contents = @"Feature: UrlVerification
	In order to demonstrate and test the use of the UrlNavigation steps
	There needs to be scenarios that cover the UrlNavigation step bindings

Scenario: Verify that the single slash (root) relative path matches the current page
	When I visit the Home page
	Then the current page should be /"
                       };

      var result = instance.GetInstance();

      result.Gherkin.Feature.Name.Should().Be("UrlVerification");
    }
  }
}
