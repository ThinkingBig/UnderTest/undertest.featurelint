using System.Collections.Generic;
using UnderTest.FeatureLint.Common.Rules;

namespace UnderTest.FeatureLint.Common.Tests.Testables
{
  public class TestableFeatureOptions : IFeatureLintConfig
  {
    public IList<string> FilePaths  { get; set; }

    public IList<string> IgnoreTags { get; set; }

    public string ExampleDataTagName { get; set; } = "@ExampleData";

    public RuleSettings RuleSettings { get; } = new RuleSettings();
  }
}
