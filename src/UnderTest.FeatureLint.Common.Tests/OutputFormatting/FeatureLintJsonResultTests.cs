using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.OutputFormatting;

namespace UnderTest.FeatureLint.Common.Tests.OutputFormatting
{
  public class FeatureLintJsonResultTests
  {
    [Test]
    public void Constructor_WhenCalled_DefaultsAreSet()
    {
      var instance = new FeatureLintJsonResult();

      instance.Successful.Should().BeFalse();
      instance.Results.Should().NotBeNull();
    }
  }
}