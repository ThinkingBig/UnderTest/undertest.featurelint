using System;
using FluentAssertions;
using NUnit.Framework;
using UnderTest.FeatureLint.Common.OutputFormatting;

namespace UnderTest.FeatureLint.Common.Tests.OutputFormatting
{
  public class JsonOutputFormatterTests
  {
    [Test]
    public void BuildSuccessResult_WhenCalled_OutputsExpectedJson()
    {
      var instance = new JsonOutputFormatter();

      var result = instance.BuildSuccessResult();

      result.Should().Be(@"{""Successful"":true,""Results"":[],""ErrorMessages"":[],""InfoMessages"":[]}");
    }

    [Test]
    public void BuildErrorResult_WhenPassedNull_ThrowsArgumentNullException()
    {
      var instance = new JsonOutputFormatter();

      Action act = () => instance.BuildErrorResult(null);

      act.Should().Throw<ArgumentNullException>();
    }

    [Test]
    public void BuildErrorResult_WhenPassed_OutputsExpectedJson()
    {
      var instance = new JsonOutputFormatter();
      var lintResult = new FeatureLintResult
      {
        Findings = {
          new FeatureLintData
          {
            Line = 1,
            Type = FeatureLintDataType.Error,
            Column = 0,
            Message = "Shoes",
            FilePath = "something.feature"
          }}
      };

      var result = instance.BuildErrorResult(lintResult);

      result.Should().Be(@"{""Successful"":false,""Results"":[{""FilePath"":""something.feature"",""Findings"":[{""FilePath"":""something.feature"",""Column"":0,""Line"":1,""Message"":""Shoes"",""Rule"":0,""Type"":1}]}],""ErrorMessages"":[],""InfoMessages"":[]}");
    }
  }
}
