# UnderTest.FeatureLint

![UnderTest](https://paper-attachments.dropbox.com/s_83978FCF913FDC2F4078354E63EC6B9F5369CBAC7A4A8452BDBD67B665B61908_1560127143078_logo1.png)
    
[![Active](http://img.shields.io/badge/Status-Active-green.svg)](https://gitlab.com/under-test/undertest) 
[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://gitlab.com/under-test/undertest.featurelint/blob/stable/LICENSE)
[![NuGet version (UnderTest.FeatureLint)](https://img.shields.io/nuget/v/UnderTest.FeatureLint.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest.FeatureLint/)
[![Build status](https://ci.appveyor.com/api/projects/status/obnec3vvmytpv70h/branch/stable?svg=true)](https://ci.appveyor.com/project/ThinkingBigRon/undertest-featurelint-ae20a)
[![NuGet version with pre-releases (UnderTest.FeatureLint)](https://img.shields.io/nuget/vpre/UnderTest.FeatureLint.svg?style=flat-square)](https://www.nuget.org/packages/UnderTest.FeatureLint/)

## What is UnderTest.FeatureLint?

Linter for gherkin feature files.

## Getting Started

1. Install the global tool via `dotnet tool install -g UnderTest.FeatureLint`
1. Invoke `featurelint *.feature` 
1. Any issues or errors will be outputted

## Configuration
 
FeatureLint comes with a default set of warnings/errors, but it was designed to be configurable. There are three ways to configure your usage of FeatureLint. The load order of precedence for options is as follows:

1. in the `$HOME/.featurelint`
2. `.featurelint` in a parent folder from `.` (`FeatureLint` starts by looking for this file in the same directory as the file that's being linted. If not found, it moves one level up the directory tree up to the filesystem root).
3. in `./.featurelint`
4. on the command line (This is not complete)

This approach allows you to have different configuration files per project. Place your file into the project root directory, and, as long as you run FeatureLint from anywhere within your project directory tree, the same configuration file used. The `.featurelint` configuration file is source control safe.

The configuration file is a simple JSON file that specifies which FeatureLint options to turn on or off and modify rules. For example, the following file adds an untestable phrase to the `BadPhrases` rule.

```
{
  "ruleSettings": {
    "badPhrases": {
      "additionalUntestable": ["because"]      
    }
  }
}
```

## Rules

Note: all rules are enabled by default.

### BadPhraseInWhenGiveThen

Checks for a list of phrases considered harmful to clarity within the team.

Type: Warning

Configuration Key: `badPhrases`

Additional properties to configure:

* `untestable` (array of strings) - replaces the default configuration list of untestable bad phrases.
* `additionalUntestable` (array of strings) - additional phrases to add to the untestable bad phrases list.
* `vaguePhrases` (array of strings) - replaces the default configuration list of vague phrases.
* `additionalVaguePhrases` (array of strings) - additional phrases to add to the vague phrases list.

### DataTableHeadersUsePascalCase

Checks that all data table headers use [PascalCase](https://en.wikipedia.org/wiki/Camel_case) naming.

Type: Warning

Configuration Key: `dataTableHeadersUsePascalCase`

### EnsureFeatureNamesAreUnique

Checks all of the features currently under examination have unique names.

Type: Error

Configuration Key: `ensureFeatureNamesAreUnique`

### EnsureScenarioNamesAreUnique

Checks all of the scenarios within a feature have unique names.

Type: Error

Configuration Key: `ensureScenarioNamesAreUnique`

### ExampleTableHeadersAreUniqueRule

Checks all table header columns are unique.

Type: Error

Configuration Key: `exampleTableHeadersAreUnique`

### ExampleTablesShouldUseLowerKebabCase

Checks all table header columns use [lower-kebab case](https://en.wikipedia.org/wiki/Snake_case).  Ex: `| order-summary |`

Type: Error

Configuration Key: `exampleTablesShouldUseLowerKebabCase`

### FeatureMustHaveDescription

Checks that all features have a description

Type: Error

Config Key: `featureMustHaveDescription`

### FeatureNameRequired

Checks all features have non-empty names. 

Type: Error

Configuration Key: `featureNameRequiredRule`

### NoDuplicateTags

Checks features and scenarios do not have duplicate tags. 

Type: Warning

Configuration Key: `noDuplicateTagsRule`

### NoDuplicateWhenGivenThen

This rule checks that there are not repeated `Given`, `When` or `Then` clauses and that instead the keywords `And` and `But` are used. 

Bad:
```gherkin
Scenario: User logs in
  Given a user
  Given the site is up
  When the user logs in
  Then the user should be logged in  
```

Good:
```gherkin
Scenario: User logs in
  Given a user
  And the site is up
  When the user logs in
  Then the user should be logged in  
```
Type: Error

Configuration Key: `noDuplicateWhenGivenThenRule`

### NoEmptyFeatureFile

This rule checks that all features have at least one scenario. 

Type: Error

Configuration Key: `noEmptyFeatureFile`

### NoEmptySteps

This rule checks that all steps have at least one statement.

Type: Error

Configuration Key: `noEmptySteps`


### NoEmptyScenario

This rule checks that all scenarios have at least one step. 

Type: Error

Configuration Key: `noEmptyScenario`

### NoPeriodsEndingLines

Checks that no lines end in a `.`

Type: Warning

Configuration Key: `noPeriodsEndingLines`

### NoRepeatKeywordsInStep

Checks for repeated keywords in steps.  eg. `When when something happens`

Type: Warning

Configuration Key: `noRepeatKeywordsInStep`

### NoTagsOnExampleTablesRule

Checks that there are no tags on `Example Tables`  

Type: Warning

Configuration Key: `noTagsOnExampleTables`

### NoThenInBackgroundsRule

Checks that there are no `Then` statements in `Background` rules.  

Type: Error

Configuration Key: `noThenInBackgrounds`

### NoUpperCaseWordsRule

Checks that no upper case words are included.  

Type: Warning

Configuration Key: `noUpperCaseWords`

Additional properties to configure:

* `whitelistedWords` (array of strings) - words which are acceptable to be included in statements

### OnlyOneWhenPerScenarioRule

Checks that there is only one `When` statement per scenario.

Type: Warning

Configuration Key: `onlyOneWhenPerScenario`

### OrderOfGivenWhenThenRule

Checks that statements within a scenario are stated in the order `Given`, `When`, `Then`.

Type: Error

Configuration Key: `orderOfGivenWhenThen`

### ScenarioNameRequiredRule

Checks that `Scenario`s are given a name.

Type: Error

Configuration Key: `scenarioNameRequired`

### ScenarioOutlineShouldHaveAnExampleTableRule

Checks that `Scenario Outline`s have an associated example table.

Type: Error

Configuration Key: `scenarioOutlineShouldHaveAnExampleTable`

### ScenarioOutlineExamplesShouldHaveANameRule

Checks that `Scenario Outline`s are given a name.

Type: Error

Configuration Key: `scenarioOutlineExamplesShouldHaveAName`

### TagCanBePromotedToTheFeatureLevel

Checks if a tag can be proomoted from all scenarios in a feature file to the feature level.

Type: Warning

Configuration Key: `tagCanBePromotedToTheFeatureLevel'`

### UnmappedExampleElementsRule

Checks that all example table values are found in an example table.

Type: Error

Configuration Key: `unmappedExampleElements`
